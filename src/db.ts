/* eslint-disable @typescript-eslint/no-explicit-any */
import { UserIdResolvable } from "@twurple/api";
import { PrismaClient, TwitchAccount } from "@prisma/client";
import { KumaUser } from "./kuma/user";
import { HelixUser } from "@twurple/api/lib";
import { default_ban_reason } from "./data/config.json";

export class Database {
    prisma: PrismaClient;

    constructor(prisma: PrismaClient) {
        this.prisma = prisma;
    }

    async getBanCount(): Promise<number> {
        return prisma.twitchAccount.count({
            where: { NOT: { ban_reason: null } },
        });
    }

    async getChannelCount(): Promise<number> {
        return prisma.twitchAccount.count({
            where: { is_lurked_channel: true },
        });
    }

    async getDoNotLockCount(): Promise<number> {
        return prisma.twitchAccount.count({ where: { is_do_not_lock: true } });
    }

    async getSubListCount(): Promise<number> {
        return prisma.twitchAccount.count({ where: { is_sublist: true } });
    }

    async getUnbanCount(): Promise<number> {
        return prisma.twitchAccount.count({ where: { is_unban: true } });
    }

    async banExists(user: KumaUser): Promise<boolean> {
        const id = user.num_id;
        return (
            (await prisma.twitchAccount.findFirst({
                where: {
                    AND: [{ id }, { NOT: [{ ban_reason: null }] }],
                },
            })) !== null
        );
    }

    async generateUpsertObject(user: KumaUser, extraKeys = {}): Promise<any> {
        const userName = await user.name;
        const profilePictureUrl = await user.profilePictureUrl;
        if (!userName || !profilePictureUrl) {
            return null;
        }
        const id = parseInt(user.num_id.toString());
        let obj = {
            id,
            name: userName,
            profilePictureUrl: profilePictureUrl,
            last_existence_check: new Date(),
        };
        obj = Object.assign(obj, extraKeys);
        return {
            create: obj,
            update: obj,
            where: { id },
        };
    }

    async addBan(user: KumaUser, reason = ""): Promise<boolean> {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            ban_reason: reason,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async addChannel(user: KumaUser): Promise<boolean> {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            is_lurked_channel: true,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async addDoNotLock(user: KumaUser): Promise<boolean> {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            is_do_not_lock: true,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async addKbdManager(user: KumaUser): Promise<boolean> {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            is_kbd_manager: true,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async addSubList(user: KumaUser, reason = "") {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            is_sublist: true,
            ban_reason: reason ?? default_ban_reason,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async addUnban(user: KumaUser) {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            is_unban: true,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async removeBan(user: KumaUser): Promise<boolean> {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            ban_reason: null,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async removeChannel(user: KumaUser): Promise<boolean> {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            is_lurked_channel: false,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async removeDoNotLock(user: KumaUser): Promise<boolean> {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            is_do_not_lock: false,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async removeKbdManager(user: KumaUser): Promise<boolean> {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            is_kbd_manager: false,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async removeSubList(user: KumaUser): Promise<boolean> {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            is_sublist: false,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async removeUnban(user: KumaUser): Promise<boolean> {
        if (!user.id && !user.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(user, {
            is_unban: false,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async getChannels(): Promise<TwitchAccount[]> {
        return prisma.twitchAccount.findMany({
            where: { is_lurked_channel: true },
            orderBy: { name: "asc" },
        });
    }

    async getChannelsByPriority(): Promise<TwitchAccount[]> {
        return prisma.twitchAccount.findMany({
            where: { is_lurked_channel: true },
            orderBy: { is_kbd_manager: "desc", name: "asc" },
        });
    }

    async getChannel(user: KumaUser): Promise<TwitchAccount | null> {
        if (!user.id && !user.num_id) {
            return null;
        }
        return prisma.twitchAccount.findUnique({
            where: { id: user.num_id },
        });
    }

    async getSubList(): Promise<TwitchAccount[]> {
        return prisma.twitchAccount.findMany({ where: { is_sublist: true } });
    }

    async getKbdManagers(): Promise<TwitchAccount[]> {
        return prisma.twitchAccount.findMany({
            where: { is_kbd_manager: true },
        });
    }

    async getTwitchAccountById(_id: number | UserIdResolvable): Promise<TwitchAccount | null> {
        const id = parseInt(String(_id));
        if (!id) {
            return null;
        }
        return (
            prisma.twitchAccount.findUnique({
                where: { id },
            }) ?? null
        );
    }

    async getTwitchAccountByName(username: string): Promise<TwitchAccount | null> {
        return prisma.twitchAccount.findUnique({
            where: { name: username },
        });
    }

    async removeTwitchAccountByName(target: string): Promise<boolean> {
        try {
            await prisma.twitchAccount.delete({
                where: { name: target },
            });
        } catch (e) {
            return false;
        }
        return true;
    }

    async getBan(id: number): Promise<TwitchAccount | null> {
        return prisma.twitchAccount.findFirst({
            where: {
                AND: [{ id }, { NOT: { ban_reason: null } }],
            },
        });
    }

    async getDoNotLockStatus(channel: KumaUser): Promise<boolean> {
        if (!channel.id || !channel.num_id) {
            return false;
        }
        const data = await prisma.twitchAccount.findFirst({
            where: {
                AND: [{ id: channel.num_id }, { is_do_not_lock: true }],
            },
        });
        if (data) {
            return data.is_do_not_lock;
        }
        return false;
    }

    async setDoNotLockStatus(channel: KumaUser, status: boolean): Promise<boolean> {
        if (!channel.id || !channel.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(channel, {
            is_do_not_lock: status,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async setProfilePicture(channel: KumaUser, url: string): Promise<boolean> {
        if (!channel.id || !channel.num_id) {
            return false;
        }
        const upsertMap = await this.generateUpsertObject(channel, {
            profilePictureUrl: url,
        });
        if (!upsertMap) {
            return false;
        }
        return (await prisma.twitchAccount.upsert(upsertMap)) !== null;
    }

    async clearSubList() {
        return prisma.twitchAccount.updateMany({
            where: { is_sublist: true },
            data: { is_sublist: false },
        });
    }

    async updateUserData(old: KumaUser, current: HelixUser) {
        return prisma.twitchAccount.update({
            where: { id: old.num_id },
            data: {
                name: current.name.toLowerCase(),
                profilePictureUrl: current.profilePictureUrl,
                last_existence_check: new Date(),
            },
        });
    }

    async updateLastExistenceCheck(user: KumaUser) {
        return prisma.twitchAccount.update({
            where: { id: user.num_id },
            data: { last_existence_check: new Date() },
        });
    }

    async getUserById(id: number) {
        return prisma.twitchAccount.findFirst({
            where: { id: id },
        });
    }

    async getUserByName(name: string) {
        return prisma.twitchAccount.findFirst({
            where: { name: name.toLowerCase() },
        });
    }

    async getAllMalsites() {
        return prisma.knownMalSites.findMany();
    }

    async getMalsite(uri: string) {
        return prisma.knownMalSites.findUnique({ where: { uri } });
    }

    async addMalsite(uri: string) {
        return prisma.knownMalSites.upsert({
            create: { uri },
            update: { uri },
            where: { uri },
        });
    }

    async removeMalsite(uri: string) {
        return prisma.knownMalSites.delete({ where: { uri } });
    }

    async getAllBans() {
        return prisma.twitchAccount.findMany({ where: { ban_reason: { not: null } } });
    }
}

const prisma = new PrismaClient();
export const connect = async () => new Database(prisma);
