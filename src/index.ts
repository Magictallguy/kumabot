import * as Sentry from "@sentry/node";
import "@sentry/tracing";

import { readFileSync, writeFile } from "fs";
import { RefreshingAuthProvider } from "@twurple/auth";

import { connect as db_connect } from "./db";
import { Controller } from "./controller";

import config from "./data/config.json";
import { log_debug } from "./utils/logging";

for (const dsn of config.sentry.dsns) {
    Sentry.init({ dsn, tracesSampleRate: 1.0 });
}

async function main() {
    // Set up the database connection
    log_debug("Connecting to database...");
    const db = await db_connect();
    // Load the auth tokens & API Client
    log_debug("Loading RefreshingAuthProvider...");
    const authProvider = new RefreshingAuthProvider({
        clientId: config.client_id,
        clientSecret: config.client_secret,
        appImpliedScopes: ["chat", "moderator"]
    });

    authProvider.onRefresh(async (userId, token) => {
        // If this refreshes anything other than the bot,
        // then you updated something and forgot about this
        if (userId !== config.bot_user) {
            throw new Error(`Bad refresh user - Expected ${config.bot_user}, got ${userId}`);
        }
        writeFile("./tokens.json", JSON.stringify(token), () => void 0);
    });

    authProvider.addUser(
        config.bot_user,
        JSON.parse(readFileSync("./tokens.json", "utf-8")),
        ["chat", "moderator"]
    );

    log_debug("Initializing controller...");
    const controller = new Controller(db, authProvider);
    await controller.init();
}

void main();
