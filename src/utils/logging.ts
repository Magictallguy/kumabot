/* eslint-disable @typescript-eslint/no-explicit-any */
import { debug } from "../data/config.json";
import colours from "../utils/colours.json";

/**
 * Returns a string with the current time in the format of `HH:MM:SS`.
 * @param date
 */
export const formatTime = (date: Date) => {
    return [
        String(date.getUTCHours()).padStart(2, "0"),
        String(date.getUTCMinutes()).padStart(2, "0"),
        String(date.getUTCSeconds()).padStart(2, "0"),
    ].join(":");
};

export const log_debug = (...args: any[]) => {
    if (debug) {
        _log("debug", ...args);
    }
};

export const log = (...args: any[]) => {
    _log("log", ...args);
};

const _log = (type: string, ...args: any[]) => {
    const currentDate = new Date();
    const currentTime = formatTime(currentDate);
    let formattedType = `${colours.fg.cyan}[LOG]${colours.reset}`;
    if (type === "debug") {
        formattedType = `${colours.fg.green}[DEBUG]${colours.reset}`;
    }
    console.log(
        `${colours.fg.gray}[${currentTime}]${colours.reset} ${formattedType}`,
        ...args.map((v) => v + colours.reset)
    );
};
