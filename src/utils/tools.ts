"use strict";
import * as linkify from "linkifyjs";
import { KumaContext } from "../kuma/context";
import { KumaUser } from "../kuma/user";
import namePatterns from "./botNamePatterns.json";
import colours from "./colours.json";
import emotes from "./emotes.json";
import { log, log_debug } from "./logging";
import spam_file from "./spam.json";
import { default_ban_reason } from "../data/config.json";

const uriCache: Map<string, string> = new Map();
export const malsites: Set<string> = new Set();

const cleanUri = (uri: string) => {
    return uri
        .replace("https://", "")
        .replace("www.", "")
        .replace(/\/+$/, "");
};

const isUriBadSite = async (
    ctx: KumaContext,
    originalUri: string,
    resolvedUri: string,
) => {
    originalUri = cleanUri(originalUri);
    resolvedUri = cleanUri(resolvedUri);
    uriCache.set(originalUri, resolvedUri);
    const result = malsites.has(originalUri) || malsites.has(resolvedUri);
    if (result) {
        if (!malsites.has(resolvedUri)) {
            await ctx.db.addMalsite(resolvedUri);
            malsites.add(resolvedUri);
        }
        if (!malsites.has(originalUri)) {
            await ctx.db.addMalsite(originalUri);
            malsites.add(originalUri);
        }
    }
    return result;
};

interface recursiveFetch {
    (uri: string, limit: number): Promise<Response>;
}

const doRecursiveFetch: recursiveFetch = (uri: string, limit: number) => fetch(uri, {
    method: "GET",
    redirect: "follow",
    referrerPolicy: "no-referrer",
}).then(res => {
    limit = limit || 2;
    if ([301, 302].includes(res.status) && limit > 0) {
        return doRecursiveFetch(res?.url ?? uri, --limit);
    }
    return res;
});

export const expandAndCheckUri = async (
    ctx: KumaContext,
    channel: KumaUser,
    user: KumaUser,
    uri: string,
): Promise<string | boolean | void> => {
    if (!uri.startsWith("http")) {
        uri = "https://" + uri;
    }
    if (uriCache.has(uri)) {
        const cachedUri = uriCache.get(uri) || "";
        if (cachedUri && (await isUriBadSite(ctx, uri, cachedUri))) {
            return await botDetectedAndBanned(ctx, channel, user);
        }
    } else {
        return await doRecursiveFetch(uri, 5)
            .then((response) => {
                const url = response.url;
                if (url.length > 0) {
                    uriCache.set(uri, url);
                }
                return url;
            })
            .then(async (url) => await isUriBadSite(ctx, uri, url))
            .then(async (answer) =>
                answer ? botDetectedAndBanned(ctx, channel, user) : false,
            )
            .catch((err) => log_debug(err));
    }
};

export const extractUris = (content: string): string[] => {
    const urlsFound = linkify.find(content);
    if (!urlsFound) {
        return [];
    }
    const capturedUris: string[] = [];
    for (const url of urlsFound) {
        if (url.type !== "url") {
            continue;
        }
        capturedUris.push(url.value);
    }
    return capturedUris;
};

export const isMessageSpam = (content: string) => {
    const spamMessages = spam_file.map(
        (v) => new RegExp(`^${v.replace(" ", "\\s*")}$`, "i"),
    );
    // We want to normalize this to remove combining marks
    let normalContent = content.normalize("NFD");
    // And remove all the diacritics
    normalContent.replace(/\p{Diacritic}/gu, "");
    // Then renormalize it in combined form, for the sake of homoglyphs
    // You're so god damn welcome Bear.
    // I love you, Feathrs <3
    normalContent = content.normalize("NFC");
    return spamMessages.some((re) => re.test(normalContent));
};

// const stripNonUTF8 = (s: string) => {
//     return decodeURI( encodeURIComponent( s ) ).replace(/[^a-zA-Z0-9-_ +]/, "");
// }

export const isBuyFollowersBot = async (
    ctx: KumaContext,
    channel: KumaUser,
    user: KumaUser,
    content: string,
) => {
    let i;
    for (i of emotes) {
        content = content.replace(i, "");
    }
    content = content.replace("\\", "\\\\");
    if (isMessageSpam(content)) {
        return true;
    }
    const urls = extractUris(content);
    if (urls) {
        for (const uri of urls) {
            await expandAndCheckUri(ctx, channel, user, uri);
        }
    }
    return false;
};

const botDetectedAndBanned = async (
    ctx: KumaContext,
    channel: KumaUser,
    user: KumaUser,
) => {
    const userName = await user.name;
    if (!userName) {
        log_debug(`Couldn't get data for ${colours.fg.yellow}${user.id}${colours.reset}`);
        return false;
    }
    const channelName = await channel.name;
    if (!channelName) {
        log_debug(`Couldn't get data for ${colours.fg.yellow}${channel.id}${colours.reset}`);
        return false;
    }
    const banResult = await ctx.trigger_ban(
        channel.id,
        user.id,
        (await user.ban_reason) ?? default_ban_reason,
    );
    let logMsg = `${channelName}> ${colours.fg.red}Bot detected: ${userName}`;
    if (banResult !== null) {
        logMsg += ` and triggered the ban`;
    }
    log(logMsg);
    return true;
};

const checkKnownBotNamePatterns = async (
    ctx: KumaContext,
    channel: KumaUser,
    user: KumaUser,
) => {
    const name = await user.name;
    if (!name) {
        return;
    }
    let found = false;
    let pattern;
    for (pattern of namePatterns) {
        const pat = new RegExp(pattern);
        if (pat.test(name)) {
            const detection = await botDetectedAndBanned(ctx, channel, user);
            if (detection) {
                found = true;
            }
        }
    }
    return found;
};

export const checkSenderForBots = async (
    ctx: KumaContext,
    channel: KumaUser,
    user: KumaUser,
) => {
    if (!user) {
        return false;
    }
    if (await checkKnownBotNamePatterns(ctx, channel, user)) {
        return true;
    }
    if (!(await user.is_banned)) {
        return false;
    }
    return await botDetectedAndBanned(ctx, channel, user);
};

export const checkMessageForSpam = async (
    ctx: KumaContext,
    channel: KumaUser,
    user: KumaUser,
    text: string,
) => {
    if (!(await isBuyFollowersBot(ctx, channel, user, text))) {
        return false;
    }
    const channelName = await channel.name;
    if (!channelName) {
        log_debug(`Couldn't get data for ${colours.fg.yellow}${channel.id}${colours.reset}`);
        return false;
    }
    const userName = await user.name;
    if (!userName) {
        log_debug(`Couldn't get data for ${colours.fg.yellow}${user.id}${colours.reset}`);
        return false;
    }
    const reason = default_ban_reason;
    await ctx.trigger_ban(channel.id, user.id, reason);
    if (!(await user.is_banned)) {
        await ctx.db.addBan(user, reason);
    }
    log(`${colours.fg.yellow}${channelName}${colours.reset}> ${colours.fg.red} Buy followers bot detected; ${colours.fg.red}${userName}`);
    return true;
};

export const getCleanNameFromInput = (name: string) => {
    return name.replace(/[^a-z0-9_]+/gi, "").toLowerCase();
};
