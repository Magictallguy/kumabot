import { ApiClient, HelixBanUser, HelixUser, UserIdResolvable } from "@twurple/api";
import { ChatClient } from "@twurple/chat";
import { LRUCache } from "lru-cache";

import { Database } from "../db";
import { log, log_debug } from "../utils/logging";
import { KumaUser } from "./user";

import { bot_user, user_cache_limit, user_name_cache_limit } from "../data/config.json";
import colours from "../utils/colours.json";

export class KumaContext {
    db: Database;
    api: ApiClient;
    chat: ChatClient;

    private _userCache: LRUCache<UserIdResolvable, KumaUser> = new LRUCache({
        max: user_cache_limit,
    });
    private _usernameCache: LRUCache<string, UserIdResolvable> = new LRUCache({
        max: user_name_cache_limit,
    });

    constructor(db: Database, api: ApiClient, chat: ChatClient) {
        this.db = db;
        this.api = api;
        this.chat = chat;
    }

    async trigger_ban(
        channel: UserIdResolvable,
        user: UserIdResolvable,
        reason: string,
    ): Promise<HelixBanUser[] | void> {
        return this.api.asUser(bot_user, async (ctx) => {
            return await ctx.moderation
                .banUser(channel, {
                    reason: reason,
                    user: user,
                })
                .catch((err) => {
                    try {
                        const content = JSON.parse(err.body);
                        log_debug(`[trigger_ban:catch] C ${channel} :: T ${user} :: M ${content.message}`);
                    } catch (e) {
                        log_debug(err, e);
                    }
                });
        });
    }

    async lock_chat(channel: KumaUser) {
        const channelName = await channel.name;
        if (!channelName) {
            log_debug(`Couldn't get data for ${colours.fg.yellow}${channel.id}${colours.reset}`);
            return;
        }
        await this.api
            .asUser(bot_user, async (ctx) => {
                await ctx.chat
                    .updateSettings(String(channel.id), {
                        emoteOnlyModeEnabled: true,
                        subscriberOnlyModeEnabled: true,
                    })
                    .catch((err) => log_debug(err));
            })
            .catch((err) => log_debug(`Couldn't get data for ${colours.fg.yellow}${err}${colours.reset}`));
        log(`${colours.fg.green}${channelName}> Locked chat`);
    }

    async unlock_chat(channel: KumaUser) {
        const channelName = await channel.name;
        if (!channelName) {
            log_debug(`Couldn't get data for ${colours.fg.yellow}${channel.id}${colours.reset}`);
            return;
        }

        await this.api
            .asUser(bot_user, async (ctx) => {
                await ctx.chat.updateSettings(String(channel.id), {
                    emoteOnlyModeEnabled: false,
                    subscriberOnlyModeEnabled: false,
                });
            })
            .catch((err) => log_debug(`Couldn't get data for ${colours.fg.yellow}${err}${colours.reset}`));

        log(`${colours.fg.green}${channelName}> Unlocked chat`);
    }

    getUserById(userId: UserIdResolvable): KumaUser {
        // We faithfully assume that this user is real.
        // Get the existing user
        let user = this._userCache.get(userId);
        if (!user) {
            // Make the user
            user = new KumaUser(this, userId, undefined, undefined, undefined);
            if (user) {
                // Add them to the cache
                this._userCache.set(user.id, user);
            }
        }

        // All yours
        return user;
    }

    getUserByHelix(helixUser: HelixUser): KumaUser {
        let user = this._userCache.get(helixUser.id);

        if (!user) {
            user = new KumaUser(
                this,
                helixUser.id,
                helixUser.name,
                helixUser,
                undefined,
            );
            if (user) {
                this._userCache.set(user.id, user);
            }
        } else {
            user.attachTwitch(helixUser);
        }

        return user;
    }

    async getUserByName(username: string): Promise<KumaUser | null> {
        // Do we know the ID for this user? Return them.
        const userId = this._usernameCache.get(username);
        if (userId) {
            return this.getUserById(userId);
        }

        const dbUser = await this.db.getUserByName(username);
        if (dbUser) {
            const dbId = <UserIdResolvable>String(dbUser.id);
            const user = new KumaUser(this, dbId, username, undefined, dbUser);
            this._userCache.set(user.id, user);
            return user;
        }

        // If we don't know the ID, get it from Twitch-proper
        const twitchUser = await this.api.users.getUserByName(username);
        if (!twitchUser) {
            return null; // There's just no such user. Fuck you.
        }

        // Because we already have the Twitch user, build manually instead of using
        // getUserById, which would be a little bit wasteful.
        const user = new KumaUser(
            this,
            twitchUser.id,
            username,
            twitchUser,
            undefined,
        );
        this._userCache.set(user.id, user);
        return user;
    }

    async trigger_unban(channelId: string, targetId: string) {
        await this.api.moderation.unbanUser(channelId, targetId).catch((e) => {
            const parsed = JSON.parse(e.body);
            const message_body = parsed.message;
            const error_code = parsed.status;
            log_debug(`[trigger_unban:catch] ${error_code} :: ${message_body} :: C ${channelId} :: T ${targetId}`);
        });
    }
}
