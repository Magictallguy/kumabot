import { TwitchAccount } from "@prisma/client";
import { HelixUser, UserIdResolvable } from "@twurple/api/lib";
import { KumaContext } from "./context";
import { log_debug } from "../utils/logging";

// Problem 1:
// Are users channels?
// According to Twitch, they ostensibly are. Nonetheless, they're semantically different,
// and the set of operations you might apply to a user and channel are different. Even
// the information you need to store about a channel or user is different. I suspect that
// at some point there is a benefit to be gained from making channels separate.
export class KumaUser {
    id: UserIdResolvable;
    private _ctx: KumaContext;
    private _twitch?: HelixUser;
    private _db?: TwitchAccount | null;
    private readonly _name?: string;

    constructor(
        ctx: KumaContext,
        id: UserIdResolvable,
        name?: string,
        twitch?: HelixUser,
        db?: TwitchAccount
    ) {
        this.id = id;
        this._name = name;

        this._ctx = ctx;
        this._db = db;
        this._twitch = twitch;
    }

    get name(): Promise<string | null> {
        return (async () => {
            if (this._name) {
                return this._name;
            }
            if (this._twitch && this._twitch.name) {
                return this._twitch.name;
            }
            if (this._db && this._db.name) {
                return this._db.name;
            }
            return (
                (await this.getDB())?.name ??
                (await this.getTwitch())?.name ??
                null
            );
        })();
    }

    get num_id(): number {
        return parseInt(String(this.id));
    }

    get profilePictureUrl(): Promise<string> {
        return (async () => {
            if (this._twitch?.profilePictureUrl) {
                return this._twitch.profilePictureUrl;
            }
            if (this._db?.profilePictureUrl) {
                return this._db.profilePictureUrl;
            }
            return (
                (await this.getDB())?.profilePictureUrl ??
                (await this.getTwitch())?.profilePictureUrl ??
                ""
            );
        })();
    }

    get is_kbd_manager(): Promise<boolean> {
        return (async () => (await this.getDB())?.is_kbd_manager ?? false)();
    }

    get ban_reason(): Promise<string | null> {
        return (async () => (await this.getDB())?.ban_reason ?? null)();
    }

    get is_lurked_channel(): Promise<boolean> {
        return (async () => (await this.getDB())?.is_lurked_channel ?? false)();
    }

    get last_existence_check(): Promise<Date | false> {
        return (async () =>
            (await this.getDB())?.last_existence_check ?? false)();
    }

    get is_banned(): Promise<boolean> {
        return (async () =>
            ((await this.getDB())?.ban_reason ?? "").length > 0)();
    }

    async getTwitch(): Promise<HelixUser | null> {
        if (this._twitch) {
            return this._twitch;
        }

        return this.forceGetTwitch();
    }

    async forceGetTwitch(): Promise<HelixUser | null> {
        const data = await this._ctx.api.users
            .getUserById(this.id)
            .catch((e) =>
                log_debug(`Failed to get twitch user ${this.id}. Error: `, e)
            );
        if (!data) {
            return null;
        }
        this._twitch = data;
        return this._twitch;
    }

    attachTwitch(twitch: HelixUser) {
        this._twitch = twitch;
    }

    async getDB(): Promise<TwitchAccount | null> {
        if (this._db !== undefined) {
            return this._db;
        }

        return this.forceGetDB();
    }

    async forceGetDB(): Promise<TwitchAccount | null> {
        if (!this.id) {
            return null;
        }
        this._db = await this._ctx.db
            .getTwitchAccountById(this.id)
            .catch(async (e) => {
                log_debug(
                    `Failed to get twitch account ${this.id}. Error: `,
                    e
                );
                return null;
            });
        return this._db;
    }
}
