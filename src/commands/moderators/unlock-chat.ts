import { CommandContext } from "../index";
import { log_debug } from "../../utils/logging";

export const aliases = ["freedom"];
export const command = "unlock-chat";
export const description = "[moderator] Disables the subscriber-only and emote-only modes on the chat.";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!ctx.isModcaster) {
        return false;
    }
    ctx.kuma
        .unlock_chat(ctx.channel)
        .then(() => void 0)
        .catch((err) => log_debug(err));

    return true;
}
