import { CommandContext } from "../index";
import { log_debug } from "../../utils/logging";

export const aliases = ["lockup", "prison", "lockdown"];
export const command = "lock-chat";
export const description = "[moderator] Enables the subscriber-only and emote-only modes on the chat.";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!ctx.isModcaster) {
        return false;
    }
    ctx.kuma
        .lock_chat(ctx.channel)
        .then(() => void 0)
        .catch((err) => log_debug(err));

    return true;
}
