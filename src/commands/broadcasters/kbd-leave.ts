import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const command = "kbd-leave";
export const description = "[broadcaster] This command allows broadcasters to undo the !join - forcing KBD to leave their channel.";

export async function execute(
    ctx: CommandContext,
    _args: string[],
): Promise<boolean> {
    if (!ctx.isBroadcaster) {
        return false;
    }
    const channelName = await ctx.channel.name;
    if (!channelName) {
        log_debug(`Channel was removed, but stopped existing?`);
        return false;
    }
    ctx.kuma.db
        .removeChannel(ctx.channel)
        .then(async () => {
            await ctx.say(`Ok, I'm going!`);
            ctx.kuma.chat.part(channelName);
            log(`Removed channel ${colours.fg.yellow}${channelName}`);
        })
        .catch(async (e) =>
            log_debug(
                `Couldn't leave ${colours.fg.yellow}${channelName}${colours.reset}. Error: `,
                e,
            ),
        );
    return true;
}
