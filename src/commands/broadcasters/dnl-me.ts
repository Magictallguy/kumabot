import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { bot_user } from "../../data/config.json";

export const aliases = ["dnl-toggle"];
export const command = "dnl-me";
export const description = "[broadcaster] Toggles the \"Do Not Lock\" flag in the given channel. Channels with the DNL flag enabled will *not* have their chats automatically locked upon stream offline after 5 minutes.";

export async function execute(
    ctx: CommandContext,
    _args: string[],
): Promise<boolean> {
    if (!ctx.isBroadcaster) {
        return false;
    }
    const channelName = await ctx.channel.name;
    if (!channelName) {
        log_debug(`Couldn't get data for ${ctx.channel}`);
        return false;
    }
    if (String(ctx.channel.id) === bot_user) {
        await ctx.say(`I can't leave my own channel!`);
        return false;
    }
    const flaggedAsDNL = await ctx.kuma.db
        .getDoNotLockStatus(ctx.channel)
        .catch(async (e) =>
            log_debug(
                `Couldn't get DNL status for ${colours.fg.yellow}${channelName}${colours.reset}. Error: `,
                e,
            ),
        );
    const config = {
        0: {
            status: true,
            disEn: "en",
            action: "Added",
        },
        1: {
            status: false,
            disEn: "dis",
            action: "Removed",
        },
    };
    const key = flaggedAsDNL ? 1 : 0;
    const conf = config[key];
    await ctx.kuma.db
        .setDoNotLockStatus(ctx.channel, conf.status)
        .then(async () => {
            await ctx.say(`Do Not Lock has been ${conf.disEn}abled.`);
            log(
                `${conf.action} Do-Not-Lock ${colours.fg.yellow}${colours.fg.yellow}${channelName}`,
            );
        })
        .catch((e) =>
            log_debug(
                `Couldn't set DNL status for ${colours.fg.yellow}${channelName}${colours.reset}. Error: `,
                e,
            ),
        );
    return true;
}
