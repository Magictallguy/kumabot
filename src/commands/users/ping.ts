import { CommandContext } from "../index";
import { log_debug } from "../../utils/logging";
import { command_prefix } from "../../data/config.json";

export const aliases = ["kumabot", "kumbot", "kbd"];
export const command = "ping";
export const description = "[all] A basic way to answer \"Is KBD responding?\". If it is, it'll respond!";

export async function execute(
    ctx: CommandContext,
    _args: string[],
): Promise<boolean> {
    ctx.say(`Pong! KumaBot Defender v${process.env.npm_package_version}${command_prefix === "dev-" ? " [dev version]" : ""} is online!`)
        .catch((err) => log_debug(err));

    return true;
}
