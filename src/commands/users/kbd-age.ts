import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const aliases = ["kbd-age"];
export const command = "kuma-age";
export const description = "[all] Responds with how old KumaBot Defender is based on it's creation date (September 24th, 2021)";

export function secondsToFull(seconds: number) {
    const years = Math.floor(seconds / 31536000);
    const months = Math.floor((seconds % 31536000) / 2628000);
    const days = Math.floor(((seconds % 31536000) % 2628000) / 86400);
    const yearOut = years > 0 ? `${years} year${years === 1 ? "" : "s"}` : "";
    const monthOut =
        months > 0 ? `${months} month${months === 1 ? "" : "s"}` : "";
    const dayOut = days > 0 ? `${days} day${days === 1 ? "" : "s"}` : "";
    const outs = [];
    if (yearOut.length > 0) {
        outs.push(yearOut);
    }
    if (monthOut.length > 0) {
        outs.push(monthOut);
    }
    if (dayOut.length > 0) {
        outs.push(dayOut);
    }
    return outs.join(", ");
}

export async function execute(
    ctx: CommandContext,
    _args: string[],
): Promise<boolean> {
    const creationDateStr = "2021-09-24";
    const creationDate = new Date(creationDateStr);
    const nowDate = new Date();
    const totalSeconds = Math.floor(
        (nowDate.getTime() - creationDate.getTime()) / 1000,
    );
    const birthday = new Date(creationDateStr);
    birthday.setFullYear(nowDate.getFullYear());
    if (creationDate > birthday) {
        birthday.setFullYear(nowDate.getFullYear() + 1);
    }
    const birthDiff = Math.floor(
        (birthday.getTime() - nowDate.getTime()) / 1000,
    );
    const content = `I was created on September 24th, 2021. I've been in service for ${secondsToFull(
        totalSeconds,
    )} and my next "Creation Celebration" is in ${secondsToFull(birthDiff)}`;
    ctx.say(content)
        .then(() => log(`${colours.bright}${content}`))
        .catch((err) => log_debug(err));

    return true;
}
