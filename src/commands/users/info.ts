import { CommandContext } from "../index";
import colours from "../../utils/colours.json";
import { log, log_debug } from "../../utils/logging";

export const aliases = ["kbd-help"];
export const command = "kbd-info";
export const description = "[all] Posts a quick sum-up of what KBD is, with a link to orsokuma.com where the rest of the relevant information can be found.";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    const channelCount = await ctx.kuma.db.getChannelCount();
    const banCount = await ctx.kuma.db.getBanCount();
    const managerCount = (await ctx.kuma.db.getKbdManagers()).length;
    ctx.say(
        `I am KumaBotDefender; an anti-bad-bot, anti-threat defense bot created by @BearOrso and currently managed by a team of ${managerCount.toLocaleString()}. ` +
        `I'm in service to ${channelCount.toLocaleString()} channels and there are ${banCount.toLocaleString()} entries on our banlist. ` +
        `For more information, including a list of my commands and who can use them, visit the creator's site: https://orsokuma.com`,
    )
        .then(() =>
            log(`Posted information about ${colours.fg.magenta}KumaBotDefender`),
        )
        .catch((err) => log_debug(err));

    return true;
}
