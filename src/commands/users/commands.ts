import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { bot_user } from "../../data/config.json";

export const aliases = ["kbd-commands"];
export const command = "commands";
export const description = "[all] Responds with where to find KBD's commands";

export async function execute(
    ctx: CommandContext,
    _args: string[],
): Promise<boolean> {
    if (String(ctx.channel.id) !== bot_user) {
        return false;
    }
    const content = `A full list of my commands are available on the creator's website: https://orsokuma.com/command-list.php`;
    ctx.say(content)
        .then(() => log(`${colours.bright}${content}`))
        .catch((err) => log_debug(err));

    return true;
}
