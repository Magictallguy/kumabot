import { CommandContext } from "../index";
import { bot_user } from "../../data/config.json";
import { log_debug } from "../../utils/logging";

export const command = "join";
export const description = "[all] This command allows users to get KBD, entirely for free. This command can be fired only in KBD's own chat.";

export async function execute(
    ctx: CommandContext,
    _args: string[],
): Promise<boolean> {
    if (String(ctx.channel.id) !== bot_user) {
        return false;
    }
    const userName = await ctx.user.name;
    if (!userName) {
        return false;
    }
    const chan = await ctx.kuma.db
        .getChannel(ctx.user)
        .catch((e) =>
            log_debug(`Couldn't get data for ${userName}. Error: `, e),
        );
    if (!chan || !chan.is_lurked_channel) {
        ctx.kuma.db
            .addChannel(ctx.user)
            .then(async () => {
                await ctx.say(
                    `Joining channel, @${userName} - please be sure to mod me! /mod KumaBotDefender`,
                );
                await ctx.kuma.chat.join(userName);
            })
            .catch(async (err) => {
                await ctx.say(`Couldn't join channel (@${userName})...`);
                log_debug(`Couldn't join channel (${userName}). Error: `, err);
            });
    } else {
        ctx.kuma.chat.part(userName);
        ctx.kuma.chat
            .join(userName)
            .then(async () => await ctx.say(`Reconnected to @${userName}`))
            .catch((err) =>
                log_debug(`Couldn't join channel (${userName}). Error: `, err),
            );
    }
    return true;
}
