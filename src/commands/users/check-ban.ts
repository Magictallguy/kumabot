// noinspection ES6PreferShortImport

import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import { getCleanNameFromInput } from "../../utils/tools";

export const aliases = [
    "whomsoever-is-banned",
    "whombstsoever-is-banned",
    "check-bans",
];
export const command = "check-ban";
export const description = "[all] Checks the given user(s) against the banlist and informs the poster whether the given user(s) is/are on the KBD banlist or not.";

export async function execute(
    ctx: CommandContext,
    _targets: string[],
): Promise<boolean> {
    if (!_targets.length) {
        await ctx.say("No user given!");
        return false;
    }
    const isBanned: string[] = [];
    const isNotBanned: string[] = [];
    const doesNotExist: string[] = [];
    for (const _target of _targets) {
        const target = getCleanNameFromInput(_target);
        const user = await ctx.kuma.getUserByName(target);
        const userName = await user?.name;
        if (!user || !userName) {
            doesNotExist.push(target);
            await ctx.kuma.db
                .removeTwitchAccountByName(target)
                .catch((e) =>
                    log_debug(`Couldn't remove ${target}. Error: `, e),
                );
            continue;
        }
        const banExists = await ctx.kuma.db.banExists(user);
        if (banExists) {
            isBanned.push(target);
        } else {
            isNotBanned.push(target);
        }
    }
    const logs = [];
    if (isBanned.length) {
        const msg = `Bans found: ${isBanned.join(", ")}`;
        await ctx.say(`🔴 ${msg} 🔴`);
        logs.push(msg);
    }
    if (isNotBanned.length) {
        const msg = `Bans not found: ${isNotBanned.join(", ")}`;
        await ctx.say(`🟢 ${msg} 🟢`);
        logs.push(msg);
    }
    if (doesNotExist.length > 0) {
        const msg = `Does not exist: ${doesNotExist.join(", ")}`;
        await ctx.say(`🟠 ${msg} 🟠`);
        logs.push(msg);
    }
    log(logs);

    return true;
}
