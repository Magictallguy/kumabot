import { CommandContext } from "../index";
import colours from "../../utils/colours.json";
import { log, log_debug } from "../../utils/logging";

export const aliases = [
    "plug-tmc",
    "tmc-pls",
    "plug-the-tmc",
    "beardiscordythingforstreamermoderatorsbutalsoanyoneiswelcomediscordthingy",
];
export const command = "tmc";
export const description = "[all] A shameless self-plug to the Twitch Moderators Community Discord server.";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    ctx.say(
        "The Twitch Moderators Community Discord: https://dsc.gg/TwitchModeratorsCommunity",
    )
        .then(() =>
            log(
                `Plugged the ${colours.fg.green}Twitch Moderators Community${colours.reset} Discord (on request)`,
            ),
        )
        .catch((err) => log_debug(err));

    return true;
}
