/* eslint-disable @typescript-eslint/no-var-requires */
import { readdirSync } from "fs";
import path from "path";

import { KumaContext } from "../kuma/context";
import { log, log_debug } from "../utils/logging";
import { admin_ids, command_prefix } from "../data/config.json";
import colours from "../utils/colours.json";
import { KumaUser } from "../kuma/user";
import { ChatMessage } from "@twurple/chat/lib/commands/ChatMessage";

export class CommandsController {
    commands: Map<string, Command> = new Map();
    command_alias: Map<string, Command> = new Map();

    constructor() {
        this.loadCommands();
    }

    getCommandFiles(commandGroup: CommandGroups): string[] {
        log_debug("Loading command files");
        const commandFiles = readdirSync(
            path.join(__dirname, commandGroup),
        ).filter((file) => file.endsWith(".js") && !file.startsWith("index."));
        log_debug(
            `${colours.fg.blue}${commandGroup} files: ${commandFiles.length}${colours.reset}`,
        );
        return commandFiles;
    }

    loadCommands() {
        log_debug("Loading commands");
        // Load all the commands. God forbid.
        // I solemnly swear, I am up to no good...
        const adminsCommands = this.getCommandFiles(CommandGroups.Admins).map(
            (file) => {
                const adminsPath = path.join(__dirname, "admins", file);
                log_debug(`${colours.fg.red}${adminsPath}${colours.reset}`);
                return require(path.join(__dirname, "admins", file)) as Command;
            },
        );
        log_debug(`Admin commands: ${adminsCommands.length}`);
        const managersCommands = this.getCommandFiles(
            CommandGroups.Managers,
        ).map((file) => {
            const managersPath = path.join(__dirname, "managers", file);
            log_debug(`${colours.fg.yellow}${managersPath}${colours.reset}`);
            return require(path.join(__dirname, "managers", file)) as Command;
        });
        log_debug(`Manager commands: ${managersCommands.length}`);
        const moderatorsCommands = this.getCommandFiles(
            CommandGroups.Moderators,
        ).map((file) => {
            const moderatorsPath = path.join(__dirname, "moderators", file);
            log_debug(`${colours.fg.green}${moderatorsPath}${colours.reset}`);
            return require(path.join(__dirname, "moderators", file)) as Command;
        });
        log_debug(`Moderator commands: ${moderatorsCommands.length}`);
        const usersCommands = this.getCommandFiles(CommandGroups.Users).map(
            (file) => {
                const usersPath = path.join(__dirname, "users", file);
                log_debug(`${colours.fg.blue}${usersPath}${colours.reset}`);
                return require(path.join(__dirname, "users", file)) as Command;
            },
        );
        log_debug(`Users commands: ${usersCommands.length}`);
        const broadcasterCommands = this.getCommandFiles(
            CommandGroups.Broadcasters,
        ).map((file) => {
            const broadcastersPath = path.join(__dirname, "broadcasters", file);
            log_debug(`${colours.fg.blue}${broadcastersPath}${colours.reset}`);
            return require(path.join(
                __dirname,
                "broadcasters",
                file,
            )) as Command;
        });
        log_debug(`Broadcaster commands: ${broadcasterCommands.length}`);
        // dirty loop - repercussions
        const commands = [
            ...adminsCommands,
            ...managersCommands,
            ...moderatorsCommands,
            ...usersCommands,
            ...broadcasterCommands,
        ];
        for (const command of commands) {
            // eslint-disable-next-line no-prototype-builtins
            if (command.hasOwnProperty("aliases")) {
                for (const alias of command.aliases) {
                    log_debug(
                        `Adding alias: ${alias}, for command: ${command.command}`,
                    );
                    this.command_alias.set(alias, command);
                }
            }
            this.commands.set(command.command, command);
        }
    }

    reloadCommands() {
        const admins = this.getCommandFiles(CommandGroups.Admins).map((v) =>
            path.join(__dirname, "admins", v),
        );
        const managers = this.getCommandFiles(CommandGroups.Managers).map((v) =>
            path.join(__dirname, "managers", v),
        );
        const moderators = this.getCommandFiles(CommandGroups.Moderators).map(
            (v) => path.join(__dirname, "moderators", v),
        );
        const users = this.getCommandFiles(CommandGroups.Users).map((v) =>
            path.join(__dirname, "users", v),
        );
        const broadcasters = this.getCommandFiles(
            CommandGroups.Broadcasters,
        ).map((v) => path.join(__dirname, "broadcasters", v));
        const modules = [
            ...admins,
            ...managers,
            ...moderators,
            ...users,
            ...broadcasters,
        ];
        for (const module of modules) {
            delete require.cache[require.resolve(module)];
        }
        this.loadCommands();
    }

    _fuck(command: string): Command {
        throw new Error(`No such command ${command}`);
    }

    parseCommand(text: string): null | [string, string[]] {
        const args = text.split(" ");
        const command = args.shift()?.toLowerCase();
        return command ? [command.replace(command_prefix, ""), args] : null;
    }

    async handleCommand(context: CommandContext, text: string) {
        context._commands = this;
        const parsed = this.parseCommand(text);
        if (!parsed) {
            // No logging? :megamind:
            return;
        }
        const [command, args] = parsed;
        if (this.commands.has(command) || this.command_alias.has(command)) {
            const channelName = await context.channel.name;
            if (!channelName) {
                log_debug(`Couldn't get data for ${context.channel}`);
                return;
            }
            try {
                void (
                    this.commands.get(command) ??
                    this.command_alias.get(command) ??
                    this._fuck(command)
                ).execute(context, args);
                log_debug(
                    `${colours.fg.magenta}${channelName}${colours.reset}> ${
                        colours.fg.green
                    }${command}${colours.reset} | ${args.join(" ")}`,
                );
            } catch (e: unknown) {
                log_debug("Command error", e);
            }
        } else {
            // not a known-to-KBD command; ignoring
        }
    }
}

export class CommandContext {
    channel: KumaUser;
    user: KumaUser;
    message: ChatMessage;
    kuma: KumaContext;
    _commands?: CommandsController;

    constructor(
        kuma: KumaContext,
        channel: KumaUser,
        user: KumaUser,
        message: ChatMessage,
    ) {
        this.channel = channel;
        this.user = user;
        this.message = message;
        this.kuma = kuma;
    }

    get isKbdAdmin(): boolean {
        return admin_ids.includes(String(this.user.id));
    }

    get isModerator(): boolean {
        return this.message.userInfo.isMod;
    }

    get isBroadcaster(): boolean {
        return this.message.userInfo.isBroadcaster;
    }

    get isModcaster(): boolean {
        return this.isModerator || this.isBroadcaster;
    }

    async isKbdManager(): Promise<boolean> {
        return this.user.is_kbd_manager;
    }

    async say(message: string): Promise<void> {
        const channelName = await this.channel.name;
        if (channelName) {
            this.kuma.chat
                .say(channelName, message)
                .then(() =>
                    log(
                        `${colours.fg.magenta}${channelName}${colours.reset}> ${colours.bg.white}${colours.fg.blue}KBD${colours.reset} said:${colours.reset} ${message}`,
                    ),
                )
                .catch((err) => log_debug(err));
        }
    }
}

export interface Command {
    command: string;
    aliases: Array<string>;

    execute(context: CommandContext, args: string[]): Promise<boolean>;
}

export enum CommandGroups {
    Admins = "admins",
    Managers = "managers",
    Moderators = "moderators",
    Users = "users",
    Broadcasters = "broadcasters",
}
