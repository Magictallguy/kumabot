import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { getCleanNameFromInput } from "../../utils/tools";

export const command = "unban-from-all";
export const description = "[manager] Unbans the given user(s) from all channels covered by KBD. This is used only when something went catastrophically wrong.";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const channelName = await ctx.channel.name;
    if (!channelName) {
        log_debug(`Couldn't get data for ${colours.fg.yellow}${channelName}`);
        return false;
    }
    if (!targets.length) {
        log_debug(
            `${colours.fg.yellow}${channelName}${colours.reset}> No targets specified`,
        );
        await ctx.say("No users given!");
        return false;
    }
    const channels = await ctx.kuma.db
        .getChannels()
        .catch((e) =>
            log_debug(`Couldn't get all channels in unban-from-all. Error: `, e),
        );
    if (!channels) {
        return false;
    }
    const channelCount = channels.length;
    const total_operations = targets.length * channelCount;
    let percentage;
    let current = 0;
    await ctx.say("Un-swinging the hammer...");
    log(
        `Unbanning ${colours.fg.cyan}${targets.length}${colours.reset} users from ${colours.fg.cyan}${channelCount}${colours.reset} channels`,
    );
    for (const channel of channels) {
        const channelId = String(channel.id);
        for (const _target of targets) {
            ++current;
            const target: string = getCleanNameFromInput(_target);
            const targetData = await ctx.kuma.getUserByName(target);
            if (!targetData) {
                continue;
            }
            percentage = Math.round((current / total_operations) * 100);
            const targetId = String(targetData.id);
            await ctx.kuma.trigger_unban(channelId, targetId);
            log(
                `${target}> ${colours.fg.green}Unbanned${colours.reset} ${
                    colours.fg.yellow
                }${target}${colours.reset} (${percentage.toFixed(3)}%)`,
            );
        }
    }
    await ctx.say("Hammer-un-swinging complete!");
    return true;
}
