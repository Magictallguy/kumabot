import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import { default_ban_reason } from "../../data/config.json";

export const aliases = ["add-sublists"];
export const command = "add-sublist";
export const description = "[manager] Adds the given user(s) to KBD's \"Sub list\", a subsection of the banlist. This is usually used when running a cleanup after a followbotting";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    let inserted: string[] = [];
    for (const _target of targets) {
        const target = _target.replace(/\W+/g, "");
        const user = await ctx.kuma.getUserByName(target);
        if (user !== null) {
            const userName = await user.name;
            if (userName) {
                const chan = await ctx.kuma.db
                    .getChannel(user)
                    .catch(async (e) => {
                        log_debug(
                            `Couldn't get data for ${target}. Error: `,
                            e,
                        );
                    });
                if (!chan || !chan.is_sublist) {
                    ctx.kuma.db
                        .addSubList(user, default_ban_reason)
                        .then(() => (inserted = [...inserted, userName]))
                        .catch((e) =>
                            log_debug(
                                `Couldn't add ${userName} to the Sublist. Error: `,
                                e,
                            ),
                        );
                }
            }
        }
    }
    if (inserted.length) {
        const count = inserted.length;
        const content = `Added ${inserted.join(
            ", ",
        )} (${count.toLocaleString()} entr${
            count === 1 ? "y" : "ies"
        }) to the Sublist`;
        await ctx.say(content);
        log(content);
    } else {
        ctx.say("All given users are already on the Sublist")
            .then(() => log(`No new entries to flag as sublist`))
            .catch((err) => log_debug(err));
    }
    return true;
}
