import { CommandContext } from "../index";
import colours from "../../utils/colours.json";
import { log, log_debug } from "../../utils/logging";

export const aliases = ["add-dnl", "add-dnls", "add-do-not-locks"];
export const command = "add-do-not-lock";
export const description = "[manager] Adds the given user(s) to KBD's \"Do Not Lock\" list";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    let inserted: string[] = [];
    for (const _target of targets) {
        const target = _target.replace(/\W+/g, "");
        const user = await ctx.kuma.getUserByName(target);
        if (user !== null) {
            const userName = await user.name;
            if (userName) {
                const chan = await ctx.kuma.db
                    .getChannel(user)
                    .catch(async (e) => {
                        log_debug(
                            `Couldn't get data for ${target}. Error: `,
                            e,
                        );
                    });
                if (!chan || !chan.is_do_not_lock) {
                    ctx.kuma.db
                        .addDoNotLock(user)
                        .then(() => (inserted = [...inserted, target]))
                        .catch((e) =>
                            log_debug(
                                `Error adding ${colours.fg.red}${userName}${colours.reset} to the Do-Not-Lock list: `,
                                e,
                            ),
                        );
                }
            }
        }
    }
    if (inserted.length > 0) {
        ctx.say(`Added ${inserted.join(", ")} to the Do-Not-Lock list`)
            .then(() =>
                log(
                    `Added ${colours.fg.yellow}${inserted.join(
                        colours.reset + ", " + colours.fg.yellow,
                    )}${colours.reset} to the Do-Not-Lock list`,
                ),
            )
            .catch((err) => log_debug(err));
    } else {
        ctx.say("All given users are already on the Do-Not-Lock list.")
            .then(() => log(`No new entries to flag as DNL`))
            .catch((err) => log_debug(err));
    }

    return true;
}
