import { CommandContext } from "../index";
import { log_debug } from "../../utils/logging";
import { getCleanNameFromInput } from "../../utils/tools";

export const command = "disconnect-from-channel";
export const aliases = [
    "disconnect-from-channels",
    "disconnect-channels",
    "disconnect-channel",
    "dc-channel",
    "dc-chan",
    "kbd-dc",
];
export const description = "[manager] This command allows KBD Managers to temporarily disconnect KBD from a channel. Primarily used for debugging purposes";

export async function execute(
    ctx: CommandContext,
    _targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!_targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    const userName = await ctx.user.name;
    if (!userName) {
        return false;
    }
    const success = [];
    const fail = [];
    for (const _target of _targets) {
        const target = getCleanNameFromInput(_target);
        const channel = await ctx.kuma
            .getUserByName(target)
            .catch((err) => log_debug(err));
        const channelName = await channel?.name;
        if (!channel || !channelName) {
            fail.push(target);
            continue;
        }
        ctx.kuma.chat.part(channelName);
        success.push(channelName);
    }
    const logMsg = [];
    if (success.length > 0) {
        logMsg.push(
            `Disconnected from ${success.length} channel${
                success.length === 1 ? "" : "s"
            }: ${success.join(", ")}`,
        );
    }
    if (fail.length > 0) {
        logMsg.push(
            `Could not disconnect from ${fail.length} channel${
                fail.length === 1 ? "" : "s"
            }: ${fail.join(", ")}`,
        );
    }
    ctx.say(logMsg.join(" "))
        .then(() => void 0)
        .catch((err) => log_debug(err));
    return true;
}
