import { CommandContext } from "../index";
import { log_debug } from "../../utils/logging";

export const command = "check-rate-limit";
export const aliases = ["check-rate-limits", "rate-limit-info"];
export const description = "[manager] This command allows KBD Managers to see the current rate limit KBD is experiencing";

export async function execute(
    ctx: CommandContext,
    _args: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    log_debug(`Getting rate limiter stats`);
    const stats = ctx.kuma.api.rateLimiterStats;
    if (!stats) {
        ctx.say(`No rate limiter stats available`)
            .then(() => void 0)
            .catch((err) => log_debug(err));
        return false;
    }
    log_debug(stats);
    ctx.say(
        `Limit: ${stats.lastKnownLimit} · Remaining: ${stats.lastKnownRemainingRequests} · Reset Date: ${stats.lastKnownResetDate}`,
    )
        .then(() => void 0)
        .catch((err) => log_debug(err));
    return true;
}
