import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const command = "clear-sublist";
export const description = "[manager] Clears the sublist of all entries.";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const count = await ctx.kuma.db.getSubListCount();
    if (count) {
        await ctx.kuma.db
            .clearSubList()
            .then(async () => {
                const countPlural = count === 1 ? "entry" : "entries";
                await ctx.say(
                    `Sublist cleared of ${count.toLocaleString()} ${countPlural}.`,
                );
                log(
                    `Cleared the Sublist of ${
                        colours.fg.cyan
                    }${count.toLocaleString()}${colours.reset} ${countPlural}.`,
                );
            })
            .catch((e) => log_debug(`Couldn't clear the Sublist. Error: `, e));
    } else {
        ctx.say(`There are no entries on the Sublist to clear`)
            .then(() =>
                log(`No entries on the Sublist to clear; doing nothing.`),
            )
            .catch((err) => log_debug(err));
    }
    return true;
}
