import { CommandContext } from "../index";
import colours from "../../utils/colours.json";
import { log, log_debug } from "../../utils/logging";
import { getCleanNameFromInput } from "../../utils/tools";

export const aliases = ["add-channels"];
export const command = "add-channel";
export const description = "[manager] Adds the given user(s) to KBD's channel lurk list and connects KBD to the given user(s) channel(s)";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    const runAdditions = async () => {
        let inserted: string[] = [];
        for (const _target of targets) {
            const target = getCleanNameFromInput(_target);
            const user = await ctx.kuma.getUserByName(target);
            if (user !== null) {
                const userName = await user.name;
                if (userName) {
                    const chan = await ctx.kuma.db
                        .getChannel(user)
                        .catch((err) =>
                            log_debug(
                                `Couldn't get data for ${target}. Error: `,
                                err,
                            ),
                        );
                    if (!chan || !chan.is_lurked_channel) {
                        await ctx.kuma.db
                            .addChannel(user)
                            .then(async () => {
                                inserted = [...inserted, target];
                                await ctx.kuma.chat.join(userName);
                            })
                            .catch(async (e) => {
                                log_debug(
                                    `Failed to add ${colours.fg.red}${target}${colours.reset} to the channel list. Error:`,
                                    e,
                                );
                            });
                    }
                }
            }
        }
        return inserted;
    };
    await runAdditions()
        .then(async inserted => {
            if (inserted.length > 0) {
                await ctx.say(`Added ${inserted.join(", ")} to the channel list.`);
                log(
                    `Added ${colours.fg.yellow}${inserted.join(
                        colours.reset + ", " + colours.fg.yellow,
                    )}${colours.reset} to the channel list.`,
                );
            } else {
                await ctx.say(
                    "All given users are either already on the channel list or they don't exist.",
                );
            }
        })

    return true;
}
