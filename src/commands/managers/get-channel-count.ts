import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const command = "get-channel-count";
export const description = "[manager] Returns a count of the entries on KBD's channel lurk list";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const count = (await ctx.kuma.db.getChannelCount()).toLocaleString();
    ctx.say(`KumaBot Defender is currently serving ${count} channels`)
        .then(() =>
            log(
                `Got the channel count (${colours.fg.cyan}${count}${colours.reset})`,
            ),
        )
        .catch((err) => log_debug(err));

    return true;
}
