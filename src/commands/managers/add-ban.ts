import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { getCleanNameFromInput } from "../../utils/tools";
import { default_ban_reason } from "../../data/config.json";

export const command = "add-ban";
export const description = "[manager] Add a user to KBD's banlist, with optional reason.";

export async function execute(
    ctx: CommandContext,
    targetAndReason: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targetAndReason.length) {
        await ctx.say("No users given!");
        return false;
    }
    const [target, _reason] = [
        getCleanNameFromInput(targetAndReason[0]),
        targetAndReason.slice(1).join(" "),
    ];
    const user = await ctx.kuma.getUserByName(target);
    const reason = _reason ?? default_ban_reason;
    if (user !== null) {
        const userName = await user.name;
        if (!userName) {
            log_debug(`Couldn't get name for ${target}.`);
            return false;
        }
        const chan = await ctx.kuma.db.getChannel(user).catch(async (e) => {
            log_debug(`Couldn't get data for channel ${target}. Error: `, e);
        });
        if (!chan || !chan.ban_reason) {
            await ctx.kuma.db.addBan(user, reason).catch(async (e) => {
                log_debug(
                    `Failed to add ${colours.fg.red}${userName}${colours.reset} to the Ban List with the reason: ${reason}. Error:`,
                    e,
                );
            });
            await ctx.kuma.trigger_ban(ctx.channel.id, user.id, reason);
            await ctx.say(`${userName} has been added to the Ban List`);
            log(
                `Added ${colours.fg.red}${userName}${colours.reset} to the Ban List with the reason: ${reason}`,
            );
        } else {
            await ctx.say(`${userName} is already on the Ban List`);
        }
    } else {
        await ctx.say(`${target} doesn't exist`);
    }

    return true;
}
