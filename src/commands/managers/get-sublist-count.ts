import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const command = "get-sublist-count";
export const description = "[manager] Returns a count of the entries on KBD's sublist";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const count = await ctx.kuma.db.getSubListCount();
    ctx.say(
        `There ${count === 1 ? "is" : "are"} ${count.toLocaleString()} entr${
            count === 1 ? "y" : "ies"
        } on the SubList`,
    )
        .then(() =>
            log(
                `Got the Sublist count (${
                    colours.fg.cyan
                }${count.toLocaleString()}${colours.reset})`,
            ),
        )
        .catch((err) => log_debug(err));

    return true;
}
