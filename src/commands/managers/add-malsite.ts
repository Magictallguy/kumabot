import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import { malsites } from "../../utils/tools";
import colours from "../../utils/colours.json";
import * as linkify from "linkifyjs";

export const aliases = [
    "add-known-malsite",
    "add-malicious-site",
    "add-malsite",
];
export const command = "add-known-malicious-site";
export const description = "[manager] Adds the given URI(s) to KBD's known malicious sites.";

export async function execute(
    ctx: CommandContext,
    args: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!args.length) {
        await ctx.say("No uris given!");
        return false;
    }
    const content = args.join(" ");
    const entries = linkify.find(content);
    if (!entries || !entries.length) {
        await ctx.say("No uris detected.");
        return false;
    }
    const urisAdded: string[] = [];
    for (const url of entries) {
        if (url.type !== "url") {
            continue;
        }
        // noinspection HttpUrlsUsage
        const uri = url.value.replace("https://", "").replace("http://", "").replace("www.", "");
        const exists = (await ctx.kuma.db.getMalsite(uri)) !== null;
        if (exists) {
            continue;
        }
        await ctx.kuma.db.addMalsite(uri);
        if (!malsites.has(uri)) {
            malsites.add(uri);
        }
        log_debug(
            `Added ${colours.fg.red}${uri}${colours.reset} to known malsites`,
        );
        urisAdded.push(uri);
    }
    const addedCount = urisAdded.length;
    const entryPlural = addedCount === 1 ? "entry" : "entries";
    await ctx.say(
        addedCount > 0
            ? `Added ${addedCount.toLocaleString()} ${entryPlural} to the Known Malicious Sites list.`
            : `All given entries are already on the Known Malicious Sites list`,
    );
    log(
        `Added ${colours.fg.blue}${addedCount.toLocaleString()}${
            colours.reset
        } ${entryPlural} to the Known Malicious Sites list.`,
    );
    return true;
}
