import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import * as linkify from "linkifyjs";
import { malsites } from "../../utils/tools";

export const aliases = [
    "remove-known-malsite",
    "remove-malicious-site",
    "remove-malsite",
];
export const command = "remove-known-malicious-site";
export const description = "[manager] Removes the given URI(s) from KBD's known malicious sites.";

export async function execute(
    ctx: CommandContext,
    args: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!args.length) {
        await ctx.say("No uris given!");
        return false;
    }
    const entries = linkify.find(args.join(" "));
    if (!entries || !entries.length) {
        await ctx.say("No uris detected.");
        return false;
    }
    const urisRemoved: string[] = [];
    for (const url of entries) {
        if (url.type !== "url") {
            continue;
        }
        // noinspection HttpUrlsUsage
        const uri = url.value.replace("https://", "").replace("http://", "").replace("www.", "");
        const exists = (await ctx.kuma.db.getMalsite(uri)) !== null;
        if (!exists) {
            continue;
        }
        await ctx.kuma.db.removeMalsite(uri);
        if (malsites.has(uri)) {
            malsites.delete(uri);
        }
        log_debug(
            `Removed ${colours.fg.red}${uri}${colours.reset} from known malsites`,
        );
        urisRemoved.push(uri);
    }
    const removedCount = urisRemoved.length;
    const entryPlural = removedCount === 1 ? "entry" : "entries";
    await ctx.say(
        removedCount > 0
            ? `Removed ${removedCount.toLocaleString()} ${entryPlural} from the Known Malicious Sites list.`
            : `None of the given entries are on the Known Malicious Sites list`,
    );
    log(
        `Removed ${colours.fg.blue}${removedCount.toLocaleString()}${
            colours.reset
        } from the Known Malicious Sites list.`,
    );
    return true;
}
