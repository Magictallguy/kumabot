import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { getCleanNameFromInput } from "../../utils/tools";
import { default_ban_reason } from "../../data/config.json";

export const command = "ban-from-all";
export const description = "[manager] Bans the given user(s) from all channels covered by KBD. This is used for the more immediate threats that are pending Twitch intervention.";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const channelName = await ctx.channel.name;
    if (!channelName) {
        log_debug(`Couldn't get data for ${colours.fg.yellow}${channelName}`);
        return false;
    }
    if (!targets.length) {
        log_debug(
            `${colours.fg.yellow}${channelName}${colours.reset}> No targets specified`,
        );
        await ctx.say("No users given!");
        return false;
    }
    ctx.kuma.db
        .getChannels()
        .then(async (channels) => {
            if (!channels) {
                log_debug(
                    `${colours.fg.yellow}${channelName}${colours.reset}> Problem! No channels found`,
                );
                return false;
            }
            const channelCount = channels.length;
            const total_operations = targets.length * channelCount;
            let percentage;
            let current = 0;
            await ctx.say("Swinging the hammer...");
            const userCache = new Map();
            log(
                `Banning ${colours.fg.cyan}${targets.length}${colours.reset} accounts from ${colours.fg.cyan}${channelCount}${colours.reset} channels`,
            );
            for (const channel of channels) {
                const channelId = String(channel.id);
                for (const _target of targets) {
                    ++current;
                    const target = getCleanNameFromInput(_target);
                    let targetData;
                    const cacheTmp = userCache.get(target);
                    if (cacheTmp) {
                        targetData = cacheTmp;
                    } else {
                        targetData = await ctx.kuma.getUserByName(target);
                        userCache.set(target, targetData);
                    }
                    if (!targetData) {
                        continue;
                    }
                    percentage = Math.round((current / total_operations) * 100);
                    const targetId = String(targetData.id);
                    await ctx.kuma.trigger_ban(channelId, targetId, default_ban_reason);
                    log(
                        `${target}> ${colours.fg.red}Banned${colours.reset} ${
                            colours.fg.yellow
                        }${target}${colours.reset} (${percentage.toFixed(3)}%)`,
                    );
                }
            }
            await ctx.say("Hammer-swinging complete!");
        })
        .catch((err) =>
            log_debug(`Couldn't get all channels in ban-from-all. Error: `, err),
        );
    return true;
}
