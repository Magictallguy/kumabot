import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const aliases = ["get-unban-count"];
export const command = "get-unbans-count";
export const description = "[manager] Returns a count of the entries on KBD's unbanlist";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const count = await ctx.kuma.db.getUnbanCount();

    ctx.say(
        `There ${count === 1 ? "is" : "are"} ${count.toLocaleString()} entr${
            count === 1 ? "y" : "ies"
        } on the Unbans List`,
    )
        .then(() =>
            log(
                `Got the unbans count (${colours.fg.cyan}${count}${colours.reset})`,
            ),
        )
        .catch((err) => log_debug(err));

    return true;
}
