import { CommandContext } from "../index";
import { log_debug } from "../../utils/logging";
import { getCleanNameFromInput } from "../../utils/tools";

export const aliases = ["remove-unbans"];
export const command = "remove-unban";
export const description = "[manager] Remove the given user(s) from KBD's unbanlist";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    let removed: string[] = [];
    for (const _target of targets) {
        const target = getCleanNameFromInput(_target);
        const user = await ctx.kuma.getUserByName(target);
        const userName = await user?.name;
        if (!user || !userName) {
            continue;
        }
        const chan = await ctx.kuma.db
            .getChannel(user)
            .catch((e) =>
                log_debug(`Couldn't get data for ${target}. Error: `, e),
            );
        if (chan && chan.is_unban) {
            await ctx.kuma.db
                .removeUnban(user)
                .then(() => (removed = [...removed, userName]))
                .catch((err) =>
                    log_debug(
                        `Couldn't remove ${userName} from the Unban List. Error: `,
                        err,
                    ),
                );
        }
    }
    if (removed.length) {
        ctx.say(
            `${removed.length} user${removed.length === 1 ? "" : "s"} removed from the Unban list`,
        )
            .catch((err) => log_debug(err));
    } else {
        ctx.say("No users removed from the Unban list")
            .then(() => void 0)
            .catch((err) => log_debug(err));
    }
    return true;
}
