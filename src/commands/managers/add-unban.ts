import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const aliases = ["add-unbans"];
export const command = "add-unban";
export const description = "[manager] Adds the given user(s) to KBD's \"Unban\" list. Similar to the sublist, this is usually used if any false positives slipped through and need unbanning.";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    let inserted: string[] = [];
    for (const _target of targets) {
        const target = _target.replace(/\W+/g, "");
        const user = await ctx.kuma.getUserByName(target);
        if (user !== null) {
            const userName = await user.name;
            if (userName) {
                const chan = await ctx.kuma.db
                    .getChannel(user)
                    .catch(async (e) => {
                        log_debug(
                            `Couldn't get data for ${target}. Error: `,
                            e,
                        );
                    });
                if (!chan || !chan.is_unban) {
                    ctx.kuma.db
                        .addUnban(user)
                        .then(() => (inserted = [...inserted, userName]))
                        .catch((e) =>
                            log_debug(
                                `Couldn't add ${userName} to the Unban List. Error: `,
                                e,
                            ),
                        );
                }
            }
        }
    }
    if (inserted.length) {
        await ctx.say(`Added ${inserted.join(", ")} to the Unban list`);
        log(
            `Added ${colours.fg.yellow}${inserted.join(
                colours.reset + ", " + colours.fg.yellow,
            )}${colours.reset} to the Unban list`,
        );
    } else {
        await ctx.say("All given users are already on the Unban list.");
    }
    return true;
}
