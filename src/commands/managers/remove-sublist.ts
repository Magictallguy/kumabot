import { CommandContext } from "../index";
import { log_debug } from "../../utils/logging";
import { getCleanNameFromInput } from "../../utils/tools";

export const aliases = ["remove-sublists"];
export const command = "remove-sublist";
export const description = "[manager] Remove the given user(s) from KBD's sublist";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    let removed: string[] = [];
    for (const _target of targets) {
        const target = getCleanNameFromInput(_target);
        const user = await ctx.kuma.getUserByName(target);
        const userName = await user?.name;
        if (!user || !userName) {
            continue;
        }
        const chan = await ctx.kuma.db
            .getChannel(user)
            .catch((err) =>
                log_debug(`Couldn't get data for ${target}. Error: `, err),
            );
        if (chan && chan.is_sublist) {
            await ctx.kuma.db
                .removeSubList(user)
                .then(() => (removed = [...removed, userName]))
                .catch((e) =>
                    log_debug(
                        `Couldn't remove ${userName} from the Sublist. Error: `,
                        e,
                    ),
                );
        }
    }
    if (removed.length) {
        ctx.say(
            `${removed.length} user${
                removed.length > 1 ? "s" : ""
            } removed from the Sublist`,
        )
            .catch((err) => log_debug(err));
    } else {
        ctx.say(`No users removed from the Sublist`)
            .then(() => void 0)
            .catch((err) => log_debug(err));
    }
    return true;
}
