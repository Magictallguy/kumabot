import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { getCleanNameFromInput } from "../../utils/tools";

// export const aliases = [""];
export const command = "update-avatars";
export const description = "[manager] Updates all avatars for use on the main site.";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        log_debug("That's a no from me");
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    let success = 0;
    let fail = 0;
    let noExist = 0;
    const noExistCap: string[] = [];
    const notCovered: string[] = [];
    for (const _target of targets) {
        const target = getCleanNameFromInput(_target);
        const targetData = await ctx.kuma.getUserByName(target);
        if (!targetData) {
            ++noExist;
            noExistCap.push(target);
            continue;
        }
        const targetName = await targetData.name;
        if (!targetName) {
            ++noExist;
            noExistCap.push(target);
            log_debug(`Couldn't find data for ${target}`);
            continue;
        }
        if (!(await targetData.is_lurked_channel)) {
            notCovered.push(target);
            ++fail;
            log_debug(
                `${colours.fg.yellow}${targetName}${colours.reset} is not covered by KBD.`,
            );
            continue;
        }
        const avatar = await targetData.profilePictureUrl;
        if (!avatar) {
            ++fail;
            continue;
        }
        await ctx.kuma.db
            .setProfilePicture(targetData, avatar)
            .catch((e) =>
                log_debug(
                    `Couldn't set the avatar of ${targetName}. Error: `,
                    e,
                ),
            );
        log(`Set the avatar of ${colours.fg.yellow}${targetName}`);
        ++success;
    }
    let response = "";
    if (success > 0) {
        response += `Successfully set ${success} avatar${
            success === 1 ? "" : "s"
        }. `;
    }
    if (fail > 0) {
        response += `Failed to set ${fail} avatar${fail === 1 ? "" : "s"}. `;
        if (notCovered.length > 0) {
            response += `The following users are not covered by KBD: ${notCovered.join(
                ", ",
            )}`;
        }
    }
    if (noExist > 0) {
        response += `${noExist} given account${
            noExist === 1 ? "" : "s"
        } don't exist. ${noExistCap.join(", ")}`;
    }
    await ctx.say(response);

    return true;
}
