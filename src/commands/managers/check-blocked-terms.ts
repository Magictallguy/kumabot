import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { bot_user } from "../../data/config.json";
import blockedTerms from "../../utils/blocked-terms.json";
import { getCleanNameFromInput } from "../../utils/tools";

export const aliases = ["term-checks"];
export const command = "term-check";
export const description = "[manager] Checks to see if known scam sites have been added as blocked terms and adds any missing ones";

export async function execute(
    ctx: CommandContext,
    _targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager()) && !ctx.isModcaster) {
        return false;
    }
    const channelName = await ctx.channel.name;
    if (!_targets.length && channelName) {
        _targets = [channelName];
    }
    for (const _target of _targets) {
        const target = getCleanNameFromInput(_target);
        const user = await ctx.kuma.getUserByName(target);
        if (user === null) {
            log(`${colours.fg.yellow}${target}${colours.reset} doesn't exist.`);
            await ctx.say(`${target} doesn't exist.`);
            return false;
        }
        const userName = await user.name;
        if (!userName) {
            await ctx.say(`${target} exists, but couldn't be verified`);
            return false;
        }
        let fails = 0;
        await ctx.kuma.api.asUser(bot_user, async (client) => {
            for (const term of blockedTerms) {
                await client.moderation
                    .addBlockedTerm(user.id, term)
                    .catch((err: Error) => {
                        ++fails;
                        log_debug(err);
                    });
            }
            return true;
        });
        const extra = channelName !== userName ? " for " + userName : "";
        const msg = fails
            ? `Nothing could be done with the blocked terms${extra}`
            : `Spam sites added as blocked terms${extra}`;
        await ctx.say(msg);
    }
    return true;
}
