import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const command = "get-manager-count";
export const description = "[manager] Returns a count of KBD's Managers";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const kbdManagers = await ctx.kuma.db
        .getKbdManagers()
        .catch((e) => log_debug(`Couldn't get KBD Manager. Error: `, e));
    if (!kbdManagers) {
        return false;
    }
    let managers: string[] = [];
    for (const manager of kbdManagers) {
        managers = [...managers, manager.name];
    }
    managers = managers.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? -1 : 1);
    const count = managers.length;
    ctx.say(
        `KumaBot Defender currently has ${count.toLocaleString()} manager${count === 1 ? "" : "s"}. They are: ${managers.join(", ")}`,
    )
        .then(() =>
            log(
                `Got the manager count (${colours.fg.cyan}${count.toLocaleString()}${colours.reset})`,
            ),
        )
        .catch((err) => log_debug(err));

    return true;
}
