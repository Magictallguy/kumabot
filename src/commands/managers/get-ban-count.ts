import { CommandContext } from "../index";
import { log } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const command = "get-ban-count";
export const description = "[manager] Returns a count of the entries on KBD's banlist";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const count = await ctx.kuma.db.getBanCount();

    await ctx.say(
        `There are ${count.toLocaleString()} entr${
            count === 1 ? "y" : "ies"
        } on the Ban List`,
    );
    log(
        `Got the Ban List count (${colours.fg.cyan}${count.toLocaleString()}${
            colours.reset
        })`,
    );

    return true;
}
