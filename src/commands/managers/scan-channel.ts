import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { bot_user, default_ban_reason } from "../../data/config.json";
import { getCleanNameFromInput } from "../../utils/tools";

export const aliases = ["scan-channels"];
export const command = "scan-channel";
export const description = "[manager] Scans the chatters of the given channel(s), compares them against the banlist, and processes accordingly";

export async function execute(
    ctx: CommandContext,
    _targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const channelName = await ctx.channel.name;
    if (!_targets.length && channelName) {
        _targets = [channelName];
    }
    for (const _target of _targets) {
        const target = getCleanNameFromInput(_target);
        const user = await ctx.kuma.getUserByName(target);
        const userName = await user?.name;
        if (!user || !userName) {
            log(`${colours.fg.yellow}${target}${colours.reset} doesn't exist.`);
            await ctx.say(`${target} doesn't exist.`);
            return false;
        }
        if (!(await user.is_lurked_channel)) {
            log(
                `${colours.fg.yellow}${userName}${colours.reset} is not covered by KumaBot Defender.`,
            );
            await ctx.say(`${userName} is not covered by KumaBot Defender.`);
            return false;
        }
        const chatters = await ctx.kuma.api.asUser(bot_user, (api) => {
            return api.chat.getChatters(user.id);
        });
        const chatterCount = chatters.data.length;
        await ctx.say(
            `Checking ${chatterCount.toLocaleString()} chatter${
                chatterCount === 1 ? "" : "s"
            } in ${userName} against the Ban List`,
        );
        log_debug(`Chatter count: ${chatterCount}`);
        let flagCount = 0;
        for (const chat of chatters.data) {
            const channel = ctx.kuma.getUserById(chat.userId);
            const channelName = (await channel?.name) ?? null;
            if (!channel || !channelName) {
                log_debug(
                    `Couldn't get data for ${colours.fg.yellow}${chat.userName}`,
                );
                continue;
            }
            if (!(await channel.is_banned)) {
                continue;
            }
            const reason = (await channel.ban_reason) ?? default_ban_reason;
            await ctx.kuma.trigger_ban(user.id, channel.id, reason);
            log(
                `${colours.fg.yellow}${channelName}${colours.reset} ${colours.fg.red}detected and banned${colours.reset} in ${colours.fg.yellow}${userName}`,
            );
            ++flagCount;
        }
        const msg =
            flagCount > 0
                ? `${flagCount} users flagged in ${userName}. 🟡`
                : `No-one in ${userName} flagged. 🟢`;
        await ctx.say(msg);
    }
    return true;
}
