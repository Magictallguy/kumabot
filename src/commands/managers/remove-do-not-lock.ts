import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const aliases = ["remove-dnl", "remove-dnls", "remove-do-not-locks"];
export const command = "remove-do-not-lock";
export const description = "[manager] Remove the given user(s) from KBD's \"Do Not Lock\" list";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    let removed: string[] = [];
    for (const _target of targets) {
        const target = _target.replace(/\W+/g, "");
        const user = await ctx.kuma.getUserByName(target);
        const userName = await user?.name;
        if (!user || !userName) {
            continue;
        }
        const chan = await ctx.kuma.db
            .getChannel(user)
            .catch((e) =>
                log_debug(`Couldn't get data for ${target}. Error: `, e),
            );
        if (chan && chan.is_do_not_lock) {
            ctx.kuma.db
                .removeDoNotLock(user)
                .then(() => (removed = [...removed, userName]))
                .catch((err) =>
                    log_debug(
                        `Couldn't remove ${userName} from the Do-Not-Lock List. Error: `,
                        err,
                    ),
                );
        }
    }
    if (removed.length) {
        ctx.say(
            `${removed.length} user${
                removed.length > 1 ? "s" : ""
            } removed from the Do Not Lock list`,
        )
            .then(() =>
                log(
                    `Removed ${colours.fg.cyan}${removed.length}${
                        colours.reset
                    } user${
                        removed.length > 1 ? "s" : ""
                    } from the Do Not Lock list`,
                ),
            )
            .catch((err) => log_debug(err));
    } else {
        ctx.say("No users removed from the Do Not Lock list")
            .then(() => void 0)
            .catch((err) => log_debug(err));
    }
    return true;
}
