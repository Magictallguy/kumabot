import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { getCleanNameFromInput } from "../../utils/tools";
import { default_ban_reason } from "../../data/config.json";

export const command = "run-sublist-in";
export const description = "[manager] Runs the sublist in the given channel(s)";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const channelName = await ctx.channel.name;
    if (!channelName) {
        log_debug(`Couldn't get data for ${colours.fg.yellow}${ctx.channel}`);
        return false;
    }
    if (!targets.length) {
        log_debug(
            `${colours.fg.yellow}${channelName}${colours.reset}> No targets specified`,
        );
        await ctx.say("No users given!");
        return false;
    }
    const sublist = await ctx.kuma.db
        .getSubList()
        .catch((err) => log_debug(`Couldn't get Sublist. Error: `, err));
    if (!sublist) {
        return false;
    }
    const total_operations = targets.length * sublist.length;
    let percentage = 0;
    let current = 0;
    if (!sublist.length) {
        log_debug("No sublist entries to process");
        return false;
    }
    log(
        `Running the Sublist (${colours.fg.cyan}${sublist.length}${colours.reset} entries) across ${colours.fg.cyan}${targets.length}${colours.reset} channels`,
    );
    await ctx.say("Hammer-swinging jobs started");
    for (const _broadcaster of targets) {
        const broadcaster = getCleanNameFromInput(_broadcaster);
        const isCoveredByKBD = await ctx.kuma.db
            .getTwitchAccountByName(broadcaster)
            .catch((e) =>
                log_debug(`Couldn't get data of ${broadcaster}. Error: `, e),
            );
        if (!isCoveredByKBD || !isCoveredByKBD?.is_lurked_channel) {
            log_debug(
                `${colours.fg.yellow}${channelName}${colours.reset} - ${broadcaster} is not covered by ${colours.fg.magenta}KumaBot Defender`,
            );
            await ctx.say(`${broadcaster} is not covered by KumaBot Defender`);
            continue;
        }
        const broadcasterData = await ctx.kuma.getUserByName(broadcaster);
        if (!broadcasterData) {
            log_debug(`Couldn't get data of ${broadcaster}`);
            continue;
        }
        const broadcasterId = parseInt(String(broadcasterData.id));

        for (const sublist_entry of sublist) {
            ++current;
            percentage = Math.round((current / total_operations) * 100);
            const userId = String(sublist_entry.id);
            await ctx.kuma
                .trigger_ban(
                    broadcasterId,
                    userId,
                    sublist_entry.ban_reason ?? default_ban_reason,
                )
                .then(() =>
                    log(
                        `${broadcaster}> ${colours.fg.red}Banned${
                            colours.reset
                        } ${colours.fg.yellow}${sublist_entry.name ?? ""}${
                            colours.reset
                        } (${percentage.toLocaleString()}%)`,
                    ),
                )
                .catch((err) => log_debug(err));
        }
    }
    await ctx.say("Hammer-swinging complete");
    return true;
}
