import { CommandContext } from "../index";
import { log_debug } from "../../utils/logging";
import { getCleanNameFromInput } from "../../utils/tools";

export const command = "reconnect-channel";
export const aliases = ["kbd-reconnect"];
export const description = "[manager] This command allows KBD Managers to force KBD to drop a channel connection and restart it. Primarily used for debugging purposes";

export async function execute(
    ctx: CommandContext,
    _targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!_targets.length) {
        ctx.say("No users given!")
            .then(() => void 0)
            .catch((err) => log_debug(err));
        return false;
    }
    const userName = await ctx.user.name;
    if (!userName) {
        return false;
    }
    const success = [];
    const fail = [];
    for (const _target of _targets) {
        const target = getCleanNameFromInput(_target);
        const channel = await ctx.kuma
            .getUserByName(target)
            .catch((err) => log_debug(err));
        const channelName = await channel?.name;
        if (!channel || !channel.is_lurked_channel || !channelName) {
            fail.push(target);
            continue;
        }

        ctx.kuma.chat.part(channelName);
        await ctx.kuma.chat
            .join(channelName)
            .then(() => void 0)
            .catch((err) => log_debug(err));
        success.push(target);
    }
    const logMsg = [];
    if (success.length > 0) {
        logMsg.push(
            `Successfully reconnected to ${success.length} channel${
                success.length === 1 ? "" : "s"
            }: ${success.join(", ")}`,
        );
    }
    if (fail.length > 0) {
        logMsg.push(
            `Failed to reconnect to ${fail.length} channel${
                fail.length === 1 ? "" : "s"
            }: ${fail.join(", ")}`,
        );
    }
    ctx.say(logMsg.join(" · "))
        .then(() => void 0)
        .catch((err) => log_debug(err));
    return true;
}
