import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { bot_user } from "../../data/config.json";

export const aliases = ["remove-channels"];
export const command = "remove-channel";
export const description = "[manager] Remove the given user(s) from KBD's channel lurk list";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    const runRemovals = async () => {
        let removed: string[] = [];
        for (const _target of targets) {
            const target = _target.replace(/\W+/g, "");
            const user = await ctx.kuma.getUserByName(target);
            if (user !== null) {
                const userName = await user.name;
                if (userName) {
                    const chan = await ctx.kuma.db
                        .getChannel(user)
                        .catch((e) =>
                            log_debug(`Couldn't get data for ${target}. Error: `, e),
                        );
                    if (
                        chan &&
                        chan.is_lurked_channel &&
                        String(chan.id) !== bot_user
                    ) {
                        if (String(chan.id) === bot_user) {
                            await ctx.say(`You can't remove KBD from itself!`);
                            continue;
                        }
                        await ctx.kuma.db
                            .removeChannel(user)
                            .then(() => {
                                removed = [...removed, userName];
                                ctx.kuma.chat.part(userName);
                            })
                            .catch((err) => log_debug(err));
                    }
                }
            }
        }
        return removed;
    };
    await runRemovals()
        .then(async removed => {
            if (removed.length > 0) {
                await ctx.say(
                    `${removed.length} user${removed.length > 1 ? "s" : ""} removed from the channel list`,
                );
                log(
                    `Removed ${colours.fg.cyan}${removed.length}${colours.reset} user${removed.length > 1 ? "s" : ""} from the channel list`,
                );
            } else {
                await ctx.say("No users removed from the channel list");
            }
        });

    return true;
}
