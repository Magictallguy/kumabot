import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { getCleanNameFromInput } from "../../utils/tools";

export const aliases = ["remove-bans"];
export const command = "remove-ban";
export const description = "[manager] Remove the given user(s) from KBD's banlist";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    let count = 0;
    for (const _target of targets) {
        const target = getCleanNameFromInput(_target);
        const user = await ctx.kuma.getUserByName(target);
        const userName = await user?.name;
        if (!user || !userName) {
            continue;
        }
        const userId = user.num_id;
        const isBanned = await ctx.kuma.db
            .getBan(userId)
            .catch((e) =>
                log_debug(`Couldn't get ban data for ${userName}. Error: `, e),
            );
        if (isBanned) {
            ++count;
            await ctx.kuma.db
                .removeBan(user)
                .catch((e) =>
                    log_debug(
                        `Couldn't remove ${userName} from the Banlist. Error: `,
                        e,
                    ),
                );
            await ctx.kuma
                .trigger_unban(String(ctx.channel.id), String(user.id))
                .catch((err) => log_debug(err));
        }
    }
    if (count > 0) {
        await ctx.say(
            `${count.toLocaleString()} user${
                count === 1 ? "" : "s"
            } removed from the ban list.`,
        );
        log(
            `Removed ${colours.fg.cyan}${count.toLocaleString()}${
                colours.reset
            } user${count === 1 ? "" : "s"} from the ban list.`,
        );
    } else {
        await ctx.say("No users found");
    }

    return true;
}
