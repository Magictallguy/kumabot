import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";

export const command = "get-dnl-count";
export const description = "[manager] Returns a count of the entries on KBD's \"Do Not Lock\" list";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const count = await ctx.kuma.db.getDoNotLockCount();
    ctx.say(
        `There ${count === 1 ? "is" : "are"} ${count.toLocaleString()} entr${
            count === 1 ? "y" : "ies"
        } on the Do Not Lock list`,
    )
        .then(() =>
            log(
                `Got the Do-Not-Lock count (${
                    colours.fg.cyan
                }${count.toLocaleString()}${colours.reset})`,
            ),
        )
        .catch((err) => log_debug(err));

    return true;
}
