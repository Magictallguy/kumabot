import { CommandContext } from "../index";
import { log_debug } from "../../utils/logging";
import {malsites} from "../../utils/tools";
export const command = "get-known-malicious-sites-count";
export const aliases = ["get-malsite-count"];
export const description = "[manager] Returns a count of the malicious site URLs known to KBD";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const count = malsites.size.toLocaleString();
    ctx.say(`KumaBot Defender knows of ${count} malicious site URL${malsites.size === 1 ? "" : "s"}`)
        .catch((err) => log_debug(err));

    return true;
}
