import { UserIdResolvable } from "@twurple/api";
import { bot_user } from "../../data/config.json";
import colours from "../../utils/colours.json";
import { log, log_debug } from "../../utils/logging";
import { getCleanNameFromInput } from "../../utils/tools";
import { CommandContext } from "../index";

export const aliases = ["mod-checks"];
export const command = "mod-check";
export const description = "[manager] Checks to see if KumaBotDefender has a moderator badge in the given channel(s)";

export async function execute(
    ctx: CommandContext,
    _targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager()) && !ctx.isModcaster) {
        return false;
    }
    const channelName = await ctx.channel.name;
    if (!_targets.length && channelName) {
        _targets = [channelName];
    }

    const swords: UserIdResolvable[] = [];
    let page;
    const swordRequest = await ctx.kuma.api.asUser(bot_user, async (runner) => {
        return runner.moderation.getModeratedChannelsPaginated(bot_user);
    });
    while ((page = await swordRequest.getNext()).length) {
        for (const usr of page) {
            swords.push(usr.name);
        }
    }
    const hasSwords: string[] = [];
    const doesNotHaveSwords: string[] = [];
    const doesntExist: string[] = [];

    log_debug(`Sword count: ${swords.length.toLocaleString()}`);

    for (const _target of _targets) {
        const target = getCleanNameFromInput(_target);
        const user = await ctx.kuma.getUserByName(target);
        const userName = await user?.name;
        if (!user || !userName) {
            log(`${colours.fg.yellow}${target}${colours.reset} doesn't exist.`);
            doesntExist.push(target);
            continue;
        }
        const hasSword = bot_user === user.id || swords.includes(userName);
        let logMsg;
        if (hasSword) {
            hasSwords.push(target);
            logMsg = `${colours.fg.green}yes${colours.reset}`;
        } else {
            doesNotHaveSwords.push(target);
            logMsg = `${colours.fg.red}no${colours.reset}`;
        }
        log(`Sword detection for KBD in ${userName}: ${logMsg}`);
    }

    if (hasSwords.length) {
        await ctx.say(`I have a sword in ${hasSwords.join(", ")} 🟢 PowerUpL SirSword PowerUpR`);
    }
    if (doesNotHaveSwords.length) {
        await ctx.say(`I don't have a sword in ${doesNotHaveSwords.join(", ")} 🟡`);
    }
    if (doesntExist.length) {
        await ctx.say(`These given users don't exist: ${doesntExist.join(", ")} 🟠`);
    }
    return true;
}
