// Look, this is just a courtesy.
import { CommandContext } from "../index";
import colours from "../../utils/colours.json";
import { log, log_debug } from "../../utils/logging";
import { default_ban_reason } from "../../data/config.json";

export const command = "add-bans";
export const description = "[manager] Adds multiple users to KBD's banlist, with default reason of \"Bot\".";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    const reason = default_ban_reason;
    const runAdditions = async () => {
        let added: string[] = [];
        let notAdded: string[] = [];
        for (const _target of targets) {
            const target = _target.replace(/\W+/g, "");
            const user = await ctx.kuma.getUserByName(target);
            if (user !== null) {
                const userName = await user.name;
                if (userName) {
                    const chan = await ctx.kuma.db
                        .getChannel(user)
                        .catch(async (e) => {
                            log_debug(
                                `Couldn't get data for ${target}. Error: `,
                                e,
                            );
                        });
                    if (!chan || !chan.ban_reason) {
                        await ctx.kuma.db.addBan(user, reason).catch(async (e) => {
                            log_debug(
                                `Failed to add ${colours.fg.red}${userName}${colours.reset} to the Ban List with the reason: ${reason}. Error:`,
                                e,
                            );
                        });
                        await ctx.kuma.trigger_ban(ctx.channel.id, user.id, reason);
                        added = [...added, userName];
                    } else {
                        notAdded = [...notAdded, userName];
                    }
                }
            }
        }
        return [added, notAdded];
    };
    await runAdditions().then(async (response) => {
        const added = response[0];
        const notAdded = response[1];
        if (added.length) {
            const addedPlural = added.length > 1 ? "s" : "";
            let msg = `${added.length} user${addedPlural} added to the Ban List`;
            if (notAdded.length) {
                const notAddedPlural = notAdded.length > 1 ? "s" : "";
                msg += `, ${notAdded.length} user${notAddedPlural} are already there`;
            }
            await ctx.say(msg);
            log(
                `Added ${colours.fg.cyan}${added.length}${colours.reset} user${addedPlural}: ${colours.fg.yellow}${added.join(colours.reset + ", " + colours.fg.yellow)}${colours.reset} to the Ban List with the reason: ${reason}`,
            );
        } else {
            await ctx.say("All specified users are already on the Ban List");
        }
    });

    return true;
}
