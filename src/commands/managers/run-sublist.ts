import { CommandContext } from "../index";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { default_ban_reason } from "../../data/config.json";

export const command = "run-sublist";
export const description = "[manager] Runs the sublist in whichever channel where this command was fired.";

export async function execute(
    ctx: CommandContext,
    _: string[],
): Promise<boolean> {
    if (!(await ctx.isKbdManager())) {
        return false;
    }
    const sublistCount = await ctx.kuma.db.getSubListCount();
    let percentage = 0;
    let current = 0;
    const sublist = await ctx.kuma.db
        .getSubList()
        .catch((e) => log_debug(`Couldn't get Sublist. Error: `, e));
    if (!sublist || !sublist.length) {
        log_debug("No sublist entries to process");
        return false;
    }
    log(
        `Running the Sublist (${colours.fg.cyan}${sublistCount}${colours.reset} entries)`,
    );
    await ctx.say("Swinging the hammer...");
    for (const entry of sublist) {
        ++current;
        percentage = Math.round((current / sublistCount) * 100);
        const channelName = await ctx.channel.name;
        if (!channelName) {
            log_debug(
                `Couldn't get data for ${colours.fg.yellow}${channelName}`,
            );
            continue;
        }
        const userId = String(entry.id);
        await ctx.kuma
            .trigger_ban(ctx.channel.id, userId, entry.ban_reason ?? default_ban_reason)
            .then(() =>
                log(
                    `${channelName}> ${colours.fg.red}Banned${colours.reset} ${
                        colours.fg.yellow
                    }${entry.name ?? "<no name>"}${
                        colours.reset
                    } (${percentage.toLocaleString()}%)`,
                ),
            )
            .catch((err) => log_debug(err));
    }
    await ctx.say("Hammer-swinging complete!");
    return true;
}
