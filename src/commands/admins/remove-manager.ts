import { CommandContext } from "../index";
import { bot_user } from "../../data/config.json";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { getCleanNameFromInput } from "../../utils/tools";

export const aliases = ["remove-managers"];
export const command = "remove-manager";
export const description = "[admin] Provides the ability to remove a Twitch user from being a KBD Manager. This will also remove the target user's moderator access from KBD's channel.";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!ctx.isKbdAdmin) {
        log("That's a no from me");
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    for (const _target of targets) {
        const target = getCleanNameFromInput(_target);
        let response = "";
        const targetData = await ctx.kuma.getUserByName(target);
        if (targetData) {
            const targetName = await targetData.name;
            if (targetName) {
                const isManager = await targetData.is_kbd_manager;
                if (isManager) {
                    const managerId = String(targetData.id);
                    await ctx.kuma.db
                        .removeKbdManager(targetData)
                        .catch(async (e) => {
                            log_debug(
                                `Couldn't remove ${targetName} as a KBD Manager: `,
                                e,
                            );
                        });
                    await ctx.kuma.api.moderation
                        .removeModerator(bot_user, managerId)
                        .then(() =>
                            log(
                                `Removed ${colours.fg.yellow}${targetName}${colours.reset} as a moderator of ${colours.fg.blue}KumaBotDefender`,
                            ),
                        )
                        .catch(async (e) => {
                            log_debug(`Couldn't unmod ${targetName}: `, e);
                        });
                    response = `Removed @${targetName} as a manager!`;
                } else {
                    response = `@${targetName} isn't a manager!`;
                }
            } else {
                log_debug(`Couldn't find data for ${target}`);
                response = `@${target} doesn't exist!`;
            }
        } else {
            response = `@${target} doesn't exist!`;
        }
        if (response) {
            await ctx.say(response);
        }
    }

    return true;
}
