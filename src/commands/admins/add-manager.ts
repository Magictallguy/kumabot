import { CommandContext } from "../index";
import { bot_user } from "../../data/config.json";
import { log, log_debug } from "../../utils/logging";
import colours from "../../utils/colours.json";
import { getCleanNameFromInput } from "../../utils/tools";

export const aliases = ["add-managers"];
export const command = "add-manager";
export const description = "[admin] Provides the ability to set a Twitch user to a KBD Manager. Using this command also grants the target user moderator access to KBD's channel.";

export async function execute(
    ctx: CommandContext,
    targets: string[],
): Promise<boolean> {
    if (!ctx.isKbdAdmin) {
        log("That's a no from me");
        return false;
    }
    if (!targets.length) {
        await ctx.say("No users given!");
        return false;
    }
    for (const _target of targets) {
        let response = "";
        const target = getCleanNameFromInput(_target);
        const targetData = await ctx.kuma.getUserByName(target);
        if (targetData) {
            const targetName = await targetData.name;
            if (targetName) {
                const isManager = await targetData.is_kbd_manager;
                if (!isManager) {
                    const managerId = String(targetData.id);
                    await ctx.kuma.db
                        .addKbdManager(targetData)
                        .catch(async (e) => {
                            log_debug(
                                `Couldn't add ${targetName} as a KBD Manager. Error: `,
                                e,
                            );
                        });
                    await ctx.kuma.api.moderation
                        .addModerator(bot_user, managerId)
                        .then(() =>
                            log(
                                `Granted ${colours.fg.yellow}${targetName}${colours.reset} moderator access to ${colours.fg.blue}KumaBotDefender`,
                            ),
                        )
                        .catch(async (e) => {
                            log_debug(`Couldn't mod ${targetName}: `, e);
                        });
                    response = `Added @${targetName} as a manager!`;
                } else {
                    response = `@${targetName} is already a manager!`;
                }
            } else {
                log_debug(`Couldn't find data for ${target}`);
                response = `@${target} is not a valid user!`;
            }
        } else {
            response = `@${target} is not a valid user!`;
        }
        if (response) {
            ctx.say(response)
                .then(() => log(`[add-manager] Sent response`))
                .catch((err) => log_debug(err));
        }
    }

    return true;
}
