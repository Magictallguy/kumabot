import { ApiClient } from "@twurple/api";
import { AuthProvider } from "@twurple/auth";
import { ChatClient } from "@twurple/chat";
import { CommandContext, CommandsController } from "./commands";
import { bot_user, debug, default_ban_reason } from "./data/config.json";
import { Database } from "./db";
import { KumaContext } from "./kuma/context";
import colours from "./utils/colours.json";
import { log, log_debug } from "./utils/logging";
import { checkMessageForSpam, checkSenderForBots, getCleanNameFromInput, malsites } from "./utils/tools";

export class Controller {
    context: KumaContext;
    commands: CommandsController = new CommandsController();
    channels: Set<string> = new Set();

    constructor(db: Database, authProvider: AuthProvider) {
        log_debug("Registering ApiClient...");
        const api = new ApiClient({ authProvider });

        log_debug("Connecting to ChatClient...");
        const chat = new ChatClient({
            authProvider,
            botLevel:
                process.env.PROJECT_ENVIRONMENT === "production"
                    ? "verified"
                    : "none",
            channels: [],
            isAlwaysMod: false,
            requestMembershipEvents: true,
            logger: {
                minLevel: debug ? "info" : "warning",
            },
        });

        log_debug("Created Kuma Context...");
        this.context = new KumaContext(db, api, chat);

        log_debug("Starting Event Listener...");

        chat.onJoinFailure((userName) => {
            log_debug(`Failed to join: ${userName}`);
        });

        chat.irc.onPasswordError((err) => {
            log_debug(`[onPasswordError] ${err.message}`);
        });

        chat.onAuthenticationSuccess(async () => {
            log_debug("Authentication success");
            log("Connecting to channels...");
            const kbd = this.context.getUserById(bot_user);
            if (kbd) {
                const kbdName = await kbd.name;
                if (kbdName) {
                    await chat
                        .join(kbdName)
                        .then(() => log("Joined KBD channel"))
                        .catch((err) => log_debug("Couldn't join KBD channel", err));
                } else {
                    log_debug(`Couldn't get name for KBD: ${bot_user}`);
                }
            }
            let connectedCount = 0;
            const channelCount = this.channels.size;
            const failedIds: string[] = [];
            const failedNames: string[] = [];
            for (const channel of this.channels) {
                const userName = await this.context.getUserById(channel).name;
                if (!userName) {
                    failedIds.push(channel);
                    log_debug(`Couldn't get name for user: ${channel}.`);
                    continue;
                }
                log(`Connecting to channel: ${userName}`);
                await chat
                    .join(userName)
                    .then(() => {
                        ++connectedCount;
                        const percentage =
                            connectedCount > 0 && channelCount > 0
                                ? (connectedCount / channelCount) * 100
                                : 0;
                        log(`Joined channel: ${userName} - (${connectedCount.toLocaleString()}/${channelCount.toLocaleString()}) (${percentage.toFixed(3)}%)`);
                    })
                    .catch((e) => {
                        failedIds.push(channel);
                        failedNames.push(userName);
                        log_debug(`Couldn't join channel: ${userName}. Error: `, e);
                    });
            }
            let msg = `${colours.fg.green}Connected to all ${channelCount.toLocaleString()} channels`;
            if (connectedCount !== channelCount) {
                const failedIdCount = failedIds.length;
                const failedNameCount = failedNames.length;
                msg = `${colours.fg.yellow}Could not connect to all channels.`;
                if (failedIdCount) {
                    msg += ` ::: Failed on ${failedIdCount.toLocaleString()} ID${failedIdCount === 1 ? "" : "s"} (${failedIds.join(", ")})`;
                    msg += this.reattemptConnections(failedIds);
                }
                if (failedNameCount) {
                    msg += ` ::: Failed on ${failedNameCount.toLocaleString()} name${failedNameCount === 1 ? "" : "s"} (${failedNames.join(", ")}).`;
                }
            }
            log(`${colours.fg.blue}Connection cycle complete.${colours.reset} ${msg}`);
        });

        chat.onMessageRatelimit((_channel, text) => {
            const channel = getCleanNameFromInput(_channel);
            if (!channel) {
                log_debug(`_channel not given. Ignoring`);
                return;
            }
            log(`${colours.fg.red}Rate limit hit while attempting to post in${colours.reset} ${colours.fg.magenta}${channel}${colours.reset} ${colours.fg.red}${text}`);
        });

        chat.onWhisper(async (_user) => {
            const user = await this.context.getUserByName(
                getCleanNameFromInput(_user),
            );
            if (!user) {
                log_debug(`Couldn't get whisper data for user: ${_user}`);
                return;
            }
            await this.context.api.whispers.sendWhisper(
                bot_user,
                user.id,
                "Sorry, KBD doesn't accept whispers. Please feel free to whisper the creator instead! @BearOrso on Twitch, or Orsokuma on Discord",
            );
        });

        chat.onJoin(async (_channel, _user) => {
            const channel = await this.context.getUserByName(
                getCleanNameFromInput(_channel),
            );
            const user = await this.context.getUserByName(
                getCleanNameFromInput(_user),
            );
            if (!channel || !user) {
                log_debug(`Couldn't get data for user: ${!channel ? _channel : _user}.`);
                return;
            }
            const channelName = await channel.name;
            const userName = await user.name;
            if (!channelName || !userName) {
                log_debug(`Couldn't get data for user: ${!channelName ? _channel : _user}.`);
                return;
            }
            if (this.channels.has(String(channel.id))) {
                const methodToUse = user.id === bot_user ? log : log_debug;
                methodToUse(`${colours.fg.blue}${_user}${colours.reset} joined ${colours.fg.yellow}${channelName}`);
            }
            if (await user.is_banned) {
                await this.context.trigger_ban(channel.id, user.id, default_ban_reason);
            }
        });

        chat.onMessage(async (_channel, _user, text, message) => {
            const chanName = getCleanNameFromInput(_channel);
            const userName = getCleanNameFromInput(_user);
            const channel = await this.context.getUserByName(chanName);
            const user =
                chanName === userName
                    ? channel
                    : await this.context.getUserByName(userName);
            if (!channel || !user) {
                log_debug(`Couldn't get data for channel: ${!channel ? _channel : _user}.`);
                return;
            }
            if (text.startsWith("!")) {
                await this.commands.handleCommand(
                    new CommandContext(this.context, channel, user, message),
                    text.slice(1),
                );
            }
            const isManager = await user.is_kbd_manager;
            const whitelist = ["streamelements", "nightbot", "wizebot", "wzbot", "frostytoolsdotcom"];
            if (!isManager && !whitelist.includes(userName.toLowerCase())) {
                if (!(await checkSenderForBots(this.context, channel, user))) {
                    await checkMessageForSpam(this.context, channel, user, text);
                }
            }
        });
    }

    private async reattemptConnections(ids: string[]) {
        const count = ids.length;
        let msg = "";
        if (count > 0) {
            msg = `. Attempting reconnection on ${count} user${count === 1 ? "" : "s"}`;
            let connectedCount = 0;
            const failed: string[] = [];
            for (const id of ids) {
                const user = await this.context.api.users.getUserById(id);
                const kuma = this.context.getUserById(id);
                if (!user) {
                    await this.context.db.removeChannel(kuma)
                        .catch(err => log_debug(err));
                } else {
                    await this.context.db.updateUserData(kuma, user)
                        .catch(err => log_debug(err));
                    await this.context.chat.join(user.name)
                        .then(() => {
                            ++connectedCount;
                            const percentage =
                                connectedCount > 0 && count > 0
                                    ? (connectedCount / count) * 100
                                    : 0;
                            log(`Joined channel: ${kuma.name} - (${connectedCount.toLocaleString()}/${count.toLocaleString()}) (${percentage.toFixed(3)}%)`);
                        })
                        .catch((e) => {
                            log_debug(`Couldn't join channel: ${user.name}. Error: `, e);
                            failed.push(user.name ?? kuma.name);
                        });
                }
            }
            const failedCount = failed.length;
            if (failedCount > 0) {
                msg += `${colours.fg.red}Re-failed connection attempt to ${failedCount} user${failedCount === 1 ? "" : "s"}: ${failed.join(", ")}`;
            }
        }
        return msg;
    }

    async loadChannels() {
        log_debug("Loading channels...");
        const channels = await this.context.db
            .getChannels()
            .catch((e) => log_debug(`${colours.fg.red}Couldn't get all channels in controller:initialize. Error: `, e));
        if (!channels) {
            return false;
        }
        for (const channel of channels) {
            const userId = String(channel.id);
            if (!this.channels.has(userId)) {
                this.channels.add(userId);
            }
        }
        log_debug(`Loaded ${this.channels.size} channels.`);
        return true;
    }

    async loadMalSites() {
        log_debug("Loading malsites...");
        const malsite_entries = await this.context.db.getAllMalsites()
            .catch((e) => log_debug(`${colours.fg.red}Couldn't get all malsites in controller:initialize. Error: `, e));
        if (!malsite_entries) {
            return false;
        }
        for (const malsite of malsite_entries) {
            if (!malsites.has(malsite.uri)) {
                malsites.add(malsite.uri);
            }
        }
        log_debug(`Loaded ${malsites.size} malsites.`);
        return true;
    }

    async init() {
        // Init! Yes bruv
        // Honestly, I added this for easier visual grepping through multiple reboots on the same console.
        const divider = `\n${colours.fg.magenta}${"=".repeat(113)}${colours.reset}\n`;
        // Don't you just love ASCII art?
        const introMessage = `${colours.fg.magenta}
|     _   __                         ______         _       ______         __                   _               |
|    | | / /                         | ___ \\       | |      |  _  \\       / _|                 | |              |
|    | |/ /  _   _  _ __ ___    __ _ | |_/ /  ___  | |_     | | | |  ___ | |_   ___  _ __    __| |  ___  _ __   |
|    |    \\ | | | || '_ \` _ \\  / _\` || ___ \\ / _ \\ | __|    | | | | / _ \\|  _| / _ \\| '_ \\  / _\` | / _ \\| '__|  |
|    | |\\  \\| |_| || | | | | || (_| || |_/ /| (_) || |_     | |/ / |  __/| |  |  __/| | | || (_| ||  __/| |     |
|    \\_| \\_/ \\__,_||_| |_| |_| \\__,_|\\____/  \\___/  \\__|    |___/   \\___||_|   \\___||_| |_| \\__,_| \\___||_|     |
|${colours.reset}                                                      ${colours.fg.cyan}v${process.env.npm_package_version}${colours.reset}                                                  ${colours.fg.magenta}|
|${colours.reset}               ${colours.fg.blue}The anti-bot moderation toolkit for Twitch, originally created by BearOrso.${colours.reset}                     ${colours.fg.magenta}|
|${colours.reset}                                               ${colours.fg.blue}https://orsokuma.com${colours.reset}                                            ${colours.fg.magenta}|${colours.reset}
`;
        log(divider, introMessage, divider);

        // Load ban, channel data
        log_debug("Starting channel load...");
        await Promise.all([this.loadChannels(), this.loadMalSites()]);

        if (!this.channels.size) {
            log_debug(`${colours.fg.red}No channels found, aborting connection.`);
            process.exit(1);
        }

        // Load chat
        log_debug("Starting ChatClient...");
        this.context.chat.connect();
    }
}
