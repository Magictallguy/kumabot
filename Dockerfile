FROM node:alpine

# Set MongoDB "working" database
ENV MONGO_INITDB_DATABASE kumabot

# Expose our MongoDB Replica Set ports
EXPOSE 27017
EXPOSE 27018

# Set working directory
WORKDIR /usr/src/kumabot

# Copy kumabot into container
COPY . /usr/src/kumabot/.
COPY ./tokens.json /usr/src/kumabot/tokens.json
COPY ./src/data/config.json /usr/src/kumabot/src/data/config.json

# Install npm packages
RUN npm i
