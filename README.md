# KumaBot Defender

Runs on Node.js.

## Install it
- `npm i`
- duplicate `.env.dist` to `.env` and configure as directed within the file
- duplicate `src/data/config.example.json` to `src/data/config.json` and configure as directed within the file
- duplicate `tokens.example.json` to `tokens.json` and follow [Twurple's OAuth walkthrough](https://twurple.js.org/docs/examples/chat/basic-bot.html) for an auto-refreshing token

## Run it
- `npm run start`

#### Addendum

Personally, I run this on an AlmaLinux 8 server and
use [pm2](https://pm2.keymetrics.io/docs/usage/process-management) to manage it.
Example:

- `cd /path/to/kumabot`
- `pm2 start npm --name kumabot run start`

If you need to start, stop or restart KumaBot Defender, use:

- `pm2 start kumabot`
- `pm2 stop kumabot`
- `pm2 restart kumabot`

## Docker
- `docker compose up` to run it
- `docker compose watch` to work on it
